# define function for gravity adjustment 
function [adjusted_absolute_gravity, adjusted_relative_gravity, gravity_diff_drift, gravity_variances, other_params] = gravity_adjust(GPs, y, t, std_obs)
    
    # sort the ground points to get the sequence
    [GPs_visited, i, j] = unique(GPs, "first");
    [_, sorted_idx] = sort(i);
    
    # find the unique visited ground points
    GPs_visited = GPs_visited(sorted_idx);
    
    # get the matrix A and set the size
    n = size(GPs_visited)(2);
    m = size(GPs)(2)-1;
    A = zeros(m, n);
    
    # fill the design matrix
    GP_id = 1;
    for gp = GPs
        if (GP_id >=2)
            idx_itr = 1;
            for gpv = GPs_visited
                if (strcmpi(gp, gpv))
                    if(idx_itr ~= 1)
                       A(GP_id-1, idx_itr-1) = 1;
                    end 
                endif
                idx_itr +=1;
            endfor
        endif
        GP_id = GP_id+1;
    endfor
    
    # get the difference elements
    y1 = y(1, 1);
    t1 = t(1, 1);
    std_obs1 = std_obs(1, 1);
    
    # define L, and fill the rest of the design matrix
    y_res = y(2:end, 1) .- y1;
    t_res = t(2:end, 1) .- t1;
    std_obs_res = (std_obs(2:end, 1)).^2 .+ std_obs1^2;
    
    # reference variance to be 1
    ref_var = 1;
    
    # get the weight matrix P
    A(:, end) = t_res;
    P = inv(diag(std_obs_res));
    
    # adjust the OEM 
    [adjusted_relative_gravity, gravity_diff_drift, gravity_variances, other_params] = adjust_observation_equations(A, y_res, P);
    
    # get the adjusted absolute value
    adjusted_absolute_gravity = [y1; adjusted_relative_gravity.+y1];
    
    return;
endfunction