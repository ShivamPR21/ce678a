function [adjusted_values, estimated_params, variances, other_params] = adjust_observation_equations(A, L, P)
    
    # define the N matrix
    N = A'*P*A;
    
    # define the U matrix
    U = A'*P*L;
    
    # get the parameter estimate 
    # the last one is drift parameter
    estimated_params = inv(N)*U;
    
    # asjusted value
    adjusted_values = A*estimated_params;
    
    # residual vector
    V = adjusted_values .- L;
    
    # define the other elements
    [n, u] = size(A);
    
    # DOF
    r = n-u;
    
    # define variance matrix
    sigma_xx = inv(N);
    sigma_vv = inv(P) .- A*inv(N)*A';
    sigma_lala = A*inv(N)*A';
    variances = {{"sigma_xx", sigma_xx},
                 {"sigma_vv", sigma_vv},
                 {"sigma_lala", sigma_lala}};
    other_params = {{"sigma_o", 1},
                    {"n", n},
                    {"u", u},
                    {"r", (n - u)},
                    {"sigma_hat_o", (V'*P*V)/r},
                    {"A", A},
                    {"P", P}};
    return;
endfunction