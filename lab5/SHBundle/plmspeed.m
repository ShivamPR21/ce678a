function p = plmspeed(l, m, thetaRAD)

% WARNING! PLMSPEED IS DEPRECATED!
% Please replace all function calls of PLMSPEED in your code by calls to
% LEGENDRE_MEX which offers a wider range of functionality:
%   old: p = plmspeed(l, thetaRAD);    --> new: p = Legendre_mex(l, thetaRAD);
%   old: p = plmspeed(l, m, thetaRAD); --> new: p = Legendre_mex(l, m, thetaRAD);
%
% PLMSPEED Fully normalized associated Legendre functions for a selected 
% order m
%
% HOW:
%    p = plmspeed(l, thetaRAD)         % assumes m = 0
%    p = plmspeed(l, m, thetaRAD)
%
% IN:
%    l ....... degree (vector). Integer, but not necessarily monotonic.
%              For l < m a vector of zeros will be returned.
%    m ....... order (scalar). If absent, m = 0 is assumed.
%    thetaRAD ... co-latitude [rad] (vector)
%
% OUT:
%    p ....... Matrix with Legendre functions. The matrix has length(thetaRAD) 
%              rows and length(l) columns, unless l or thetaRAD is scalar. Then 
%              the output vector follows the shape of respectively l or thetaRAD. 
% 
% SEE ALSO:
%    LEGPOL, YLM

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Nico SNEEUW (NS), IAPG, TU-Munich
%    Markus ANTONI (MA), GI, Uni Stuttgart 
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2014-10-08: MR, declare function deprecated
%    2014-01-13: MR, review help text
%    2013-02-13: MR, change function name to show the use of radians, 
%                    brush up comments
%    2004-11-24: MW, speed up calculation
%    2004-08-13: MW, extension for first derivative
%    1999-02-??: NS, further help text brush-up
%    1998-07-13: NS, Pmm non-recursive anymore
%    1997-06-09: NS, help text brushed up
%    1994-08-08: NS, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

% Some input checking.
if nargin == 2
   thetaRAD = m;
   m  = 0;
end
if isvector(l) ~= 1,   error('Degree l must be vector (or scalar)'), end
if any(rem(l,1) ~= 0), error('Vector l contains non-integers.'), end
if isscalar(m) ~= 1,   error('Order m must be scalar.'), end
if rem(m,1) ~=0,       error('Order m must be integer.'), end

% Preliminaries.
lmax = max(l);
if lmax < m, error('Largest degree still smaller than order m.'), end
n    = length(thetaRAD);				% number of latitudes
thetaRAD    = thetaRAD(:);
x    = cos(thetaRAD);
y    = sin(thetaRAD);

% Recursive computation of the temporary matrix ptmp, containing the Legendre
% functions in its columns, with progressing degree l. The last column of
% ptmp will contain zeros, which is useful for assignments when l < m.
p = zeros(n,lmax-m+1);

%--------------------------------------------------------------------
% sectorial recursion: PM (non-recursive, though)
%--------------------------------------------------------------------
if m == 0
   fac = 1;
else
   mm  = 2*(1:m);
   fac = sqrt(2*prod((mm+1)./mm));
end

dy = 1;
for i = 1:m, dy = dy.*y; end
p(:,1) = fac*dy;

%--------------------------------------------------------------------
% l-recursion: P
%--------------------------------------------------------------------
for l = m+1:lmax
   col   = l - m + 1;			% points to the next column of ptmp
   root1 = sqrt( (2*l+1)*(2*l-1)/((l-m)*(l+m)) ) ;
   root2 = sqrt( (2*l+1)*(l+m-1)*(l-m-1) / ( (2*l-3)*(l-m)*(l+m) ) );

   % recursion
   if l == m+1
       p(:,col) = root1 *x.*p(:,col-1);
   else
       p(:,col) = root1 *x.*p(:,col-1) - root2 *p(:,col-2);
   end
       
end
