function f = gshsptw(field, lamRAD, phiRAD, r, ldesired, quant, jflag, const)

% GSHSPTW calculates a pointwise global spherical harmonic synthesis. 
%
% f = gshsptw(field, lamRAD, phiRAD, r, ldesired, quant, jflag, const)
%
% IN:
%    field ..... gravity field in |c\s| or /s|c\ format
%    lamRAD .... longitude [rad]                                    [n, 1]
%    phiRAD .... latitude  [rad]                                    [n, 1]
%    r ......... radius    [m]                                      [1, 1]
%    ldesired .. (optional) maximum degree                          [1, 1]
%    quant ..... (optional) string, defining the field quantity:
%                - 'potential' .. (default), potential [m^2/s^2]
%                - 'tr' ......... gravity disturbance [mGal]
%                - 'trr' ........ 2nd rad. derivative [E]
%                - 'none' ....... coefficients define the output
%                - 'geoid' ...... geoid height [m] 
%                - 'dg' ......... gravity anomaly [mGal]
%                - 'slope' ...... slope [arcsec]
%                - 'water' ...... equivalent water thickness [m] 
%                - 'smd' ........ surface mass density
%    jflag ..... (default: true), determines whether WGS84 is subtracted.
%    const ..... constants loaded for the calculations               [1, 2]
%
% OUT:
%    f ......... field quantity                                      [n, 1]
%
% EXAMPLE: see SHBUNDLE/example/example_gshsptw.m
%
% USES:
%    normalklm, cs2sc, sc2cs, plm, lovenr, UBERALL/checkcoor, 
%    UBERALL/constants
%

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Matthias WEIGELT (MW), DoGE, UofC
%    Markus ANTONI (MA), GI, Uni Stuttgart 
%    Matthias ROTH (MR), GI, Uni Stuttgart
% -------------------------------------------------------------------------
% revision history:
%    2018-11-27: MA, extra warning if a reference field is subtracted
%                    (request of external users)
%    2014-11-05: MR, revise help text, fork to gshs_plm, declare deprecated
%    2014-01-15: MR, revise help text and code
%    2013-02-13: MR, change function names, brush up comments
%    2013-01-30: MA, comments/removing of smoothing option
%    2013-01-23: MA, input in radian
%    2009-03-04: MW, change input co-latitude -> latitude
%    2005-10-25: MW, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse GSHS_PTW instead. \n--> Please check the help to this function and update your code!');

%% INPUT CHECK and PREPARATION
narginchk(4, 8); 
if nargin < 8 || isempty(const), const = [];          end
if nargin < 7 || isempty(jflag), jflag = 1;           end
if nargin < 6 || isempty(quant), quant = 'potential'; end

% load necessary constants
% -------------------------
if isempty(const)
    constants;
elseif numel(const) == 2
    GM = const(1);
    ae = const(2);
else
    eval([const ';']);
end

% Size determination for field
[row, col] = size(field);
if (row ~= col) && (col ~= 2*row-1)
   error('Input ''field'' not in cs or sc format');
elseif col ~= row
    % if data is in cs-format we transfer it to sc-format
   field = sc2cs(field);
   row   = size(field,1);
end
lmax = row - 1;

% prepare the coordinates
[lamRAD, phiRAD, r] = checkcoor(lamRAD, phiRAD, r, ae, 'pointwise');
theRAD              = (pi/2 - phiRAD);
didx = 1:numel(lamRAD);

% use the desired degree
if nargin < 5 || isempty(ldesired), ldesired = lmax; end
if ldesired < lmax,
    field = field(1:ldesired+1, 1:ldesired+1);
    lmax  = ldesired;
end

% rearrange field and substract reference field
field  = cs2sc(field);
row    = size(field, 1);
if jflag, field = field - cs2sc(full(normalklm(lmax, 'wgs84'))); 
    warning('A reference field (WGS84) is removed from your coefficients')
end

% prepare cosine and sine --> cos(m*lam) and sin(m*lam)
m       = (0:lmax);
l       = m';

% apply transfer function
if strcmp(quant,'none')
    tk = ones(size(l));                 % []
    tf = ones(size(l));         
    tl = l;
elseif strcmp(quant,'potential')
    tk = GM./ae;	               		% [m^2/s^2]
    tf = ones(size(l));	
    tl = l+1;
elseif strcmp(quant,'geoid')
    tk = ae;                            % [m]
    tf = ones(size(l));
    tl = l-1;
elseif strcmp(quant,'gravity') || strcmp(quant,'dg')
    tk = GM/ae/ae * 1e5; 			    % [mGal]
    tf = l-1;
    tl = l+2;
elseif strcmp(quant,'tr')
    tk = -GM/ae/ae * 1e5;               % [mGal]    
    tf = l+1;	
    tl = l+2;
elseif strcmp(quant,'trr')
    tk = GM/ae/ae/ae * 1e9;	            % [E]
    tf = (l+1).*(l+2);	
    tl = l+3;
elseif strcmp(quant,'slope')
    tk = ones(size(l));                 % [rad]
    tf = sqrt(l.*(l+1));				
    tl = l;
elseif strcmp(quant,'water')
    tk = ae * 5.517 / 3;                % [m]
    tf = (2.*l+1)./(1+lovenr(l));
    tl = l-1;
elseif strcmp(quant,'smd')
    tk = ae * 5517 / 3;                 % [kg/m^2]
    tf = (2.*l+1)./(1+lovenr(l));
    tl = l-1;
else
    error('Requested functional QUANT not available.')
end
 
field = field .* ((tk.*tf(:)) * ones(1, 2*lmax+1));
 
%% CALCULATION
f  = NaN(size(lamRAD));

% cut data in parts of 1500 elements
maxlength = 1500;

% prepare index
idx    = all(~isnan([lamRAD theRAD r]), 2);
didx   = didx(idx);
lamRAD = lamRAD(idx);
theRAD = theRAD(idx);
r      = r(idx);

% calculation
parts = ceil(numel(lamRAD) / maxlength);
for ridx = 1:parts
    idx     = (ridx-1) * maxlength + 1:min(ridx*maxlength, numel(lamRAD));
    
    % prepare cosine and sine --> cos(m*lam) and sin(m*lam)
    mlam    = l*lamRAD(idx)';         % matrix of size(length(m),length(lam))
    cosmlam = cos(mlam);
    sinmlam = sin(mlam);
    
    % prepare ratio Earth radius over radius
    TF = bsxfun(@power, ae./r(idx), tl');
    TA = zeros(length(idx),row);
    TB = zeros(length(idx),row);
    for m = 0:row-1
        Cnm = field(:, row + m);           % get Cnm coefficients for order m
        if m == 0
            Snm = zeros(row, 1);           % there are no Sn0 coefficients
        else
            Snm = field(:, row - m);       % get Snm coefficients for order m
        end

        P = plm(l, m, theRAD(idx));        % calc fully normalized Legendre Polynoms 
        
        if numel(theRAD(idx)) == 1, 
            P = P'; 
        end
        % ------------------------------------------------------------------
        TA(:, m+1) = (TF .* P) * Cnm;
        TB(:, m+1) = (TF .* P) * Snm;
    end
    
    % now do the final summation
    f(didx(idx)) = sum(TA .* cosmlam' + TB .* sinmlam', 2);
end


