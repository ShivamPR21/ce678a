function f = gshsag(field, lamRAD, phiRAD, h, ldesired, quant, jflag, const, curv)

% GSHSAG calculates a global spherical harmonic synthesis for any grid 
% defined by lam and phi (both vectors). The radius must be scalar.
%
% f = gshsag(field,lamRAD,phiRAD,h,lmax,quant,jflag,const,curv)
%
% IN:
%    field ... gravity field in |c\s| or /s|c\ format
%    lamRAD .. longitude [rad]                                      [n, 1]
%    phiRAD .. latitude  [rad]                                      [m, 1]
%    h ....... height    [m]                                        [1, 1]
%    lmax .... maximum degree (optional)                            [1, 1]
%    quant ... optional argument, defining the field quantity:    [string]
%              - 'potential' .. (default), potential [m^2/s^2]
%              - 'tr' ......... gravity disturbance [mGal]
%              - 'trr' ........ 2nd rad. derivative [E]
%              - 'none' ....... coefficients define the output
%              - 'geoid' ...... geoid height [m] 
%              - 'dg' ......... gravity anomaly [mGal]
%              - 'slope' ...... slope [arcsec]
%              - 'water' ...... equivalent water thickness [m] 
%              - 'smd' ........ surface mass density
%                        default: 'potential'
%    jflag ... (default: true) determines whether WGS84 is subtracted.
%    const ... type of constants loaded for the calculations      [string]
%    curv .... curvature of the chosen quantity                     [bool]
%
% OUT:
%    f ....... field quantity                                       [n, m]
%
% EXAMPLE:
%    load egm96; lmax = 200;
%    field =  vec2cs(lmax,EGM96(:,3),EGM96(:,4))
%    theRAD = [0:.01:pi],lamRAD = [0:.01:2*pi]
%    [V_dg] = gshsag(field,lamRAD,pi/2-theRAD,0,lmax,'dg');
%    [V_rr] = gshsag(field,lamRAD,pi/2-theRAD,300,lmax,'trr');
%    figure;
%    subplot(221);
%    imagesc(lamRAD*180/pi,theRAD*180/pi, V_dg); title('grav. anomaly')
%    subplot(222);
%    imagesc(lamRAD*180/pi,theRAD*180/pi, V_rr); title('2nd radial derivative')
%
% USES:
%    vec2cs, normalklm, eigengrav, plm, cs2sc, sc2cs, 
%    uberall/constants, uberall/checkcoor
%
% SEE ALSO:
%    GSHS, RSHS, GSHA
%
% REMARKS:
%    - h must be scalar!

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Matthias WEIGELT (MW), DoGE, UofC
%    Markus ANTONI (MA), GI, Uni Stuttgart 
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2018-11-27: MA, extra warning if a reference field is subtracted
%                    (request of external users)
%   2015-05-22: MR, declare deprecated, use 'gshs_grid' instead
%   2014-01-15: MR, revise help text, beautify code
%   2013-02-13: MR, change function names, brush up comments
%   2013-01-30: MA, comments/removing of smoothing option
%   2013-01-23: MA, input of plm in radian
%   2012-03-06: MW, add curvature calculations
%   2009-03-04: MW, change input co-latitude -> latitude
%   2005-10-25: MW, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse GSHS_GRID instead. \n--> Please check the help to this function and update your code!');

%--------------------------------------------------------------------------
% INPUT CHECK and PREPARATION
%--------------------------------------------------------------------------
narginchk(4, 9); 
if nargin < 9 || isempty(curv),  curv  = false;       end
if nargin < 8 || isempty(const), const = [];          end
if nargin < 7 || isempty(jflag), jflag = true;        end
if nargin < 6 || isempty(quant), quant = 'potential'; end


% load necessary constants
% -------------------------
if isempty(const)
    constants;
elseif numel(const) == 2
    % do nothing
else
    eval([const ';']);
end

% Size determination for field
[row, col] = size(field);
if (row ~= col) && (col ~= 2*row-1)
   error('Input ''field'' not in cs or sc format');
elseif col ~= row
    % if data is in cs-format we transfer it to sc-format
   field = sc2cs(field);
   row   = size(field, 1);
end
lmax = row - 1;

% check if r, lam and th are vectors
[lamRAD, phiRAD, ~] = checkcoor(lamRAD, phiRAD, 0, 0, 'grid');
theRAD   = (pi/2 - phiRAD);
if numel(h)~=1 && numel(h)~=0
    error('''h'' must be scalar.');
end

% use the desired degree
if nargin < 5 || isempty(ldesired), ldesired = lmax; end
if ldesired < lmax,
    field = field(1:ldesired+1, 1:ldesired+1);
    lmax  = ldesired;
end

% rearrange field and substract reference field
field  = cs2sc(field);
row    = size(field, 1);
if jflag
    field = field - cs2sc(full(normalklm(lmax, 'wgs84'))); 
    warning('A reference field (WGS84) is removed from your coefficients')
end

% prepare cosine and sine --> cos(m*lam) and sin(m*lam)
m       = (0:lmax);
l       = m';
mlam    = (lamRAD*m)';           
cosmlam = cos(mlam);
sinmlam = sin(mlam);

% apply transfer function
transf  = eigengrav(l, quant, h, const);
field   = diag(transf(:)) * field;
 

%----------------------------------------------------------------------------
% CALCULATION
%----------------------------------------------------------------------------
if curv
    TAp  = zeros(length(theRAD), row);  TBp  = TAp;
    TAl  = TAp;                         TBl  = TAp;
    TApl = TAp;                         TBpl = TAp;
    TApp = TAp;                         TBpp = TAp;
    TAll = TAp;                         TBll = TAp;
    for m = 0:row-1
        Cnm = field(:, row + m);              % get Cnm coefficients for order m
        if m == 0
            Snm = zeros(row, 1);              % there are no Sn0 coefficients
        else
            Snm = field(:, row - m);          % get Snm coefficients for order m
        end
        [P, dP, ddP] = plm(l, m, theRAD); % calc fully normalized Legendre Polynoms
        % ------------------------------------------------------------------
        TAp(:, m+1)  = -dP * Cnm;  TBp(:, m+1)  = -dP * Snm;
        TAl(:, m+1)  =   P * Snm;  TBl(:, m+1)  =  -P * Cnm;
        TApp(:, m+1) = ddP * Cnm;  TBpp(:, m+1) = ddP * Snm;
        TAll(:, m+1) =  -P * Cnm;  TBll(:, m+1) =  -P * Snm;
        TApl(:, m+1) = -dP * Snm;  TBpl(:, m+1) =  dP * Cnm;
    end
    
    % sum the partial derivatives
    fp  = TAp * cosmlam  + TBp * sinmlam; 
    fl  = TAl * cosmlam  + TBl * sinmlam;
    fpp = TApp * cosmlam + TBpp * sinmlam;
    fll = TAll * cosmlam + TBll * sinmlam;
    fpl = TApl * cosmlam + TBpl * sinmlam;
    
    % now do the final summation
    f   = ((1+fp.^2).*fll - 2.*fp.*fl.*fpl + (1+fl.^2).*fpp) ./ (2.*(realsqrt(1 + fp.^2 + fl.^2)).^3);
    
else
    TA = zeros(length(theRAD), row);
    TB = TA;
    for m = 0:row-1
        Cnm = field(:, row + m);         % get Cnm coefficients for order m
        if m == 0
            Snm = zeros(row, 1);         % there are no Sn0 coefficients
        else
            Snm = field(:, row - m);     % get Snm coefficients for order m
        end
        P = plm(l, m, theRAD);       % calc fully normalized Legendre Polynoms
        % ------------------------------------------------------------------
        TA(:, m+1) = P * Cnm;
        TB(:, m+1) = P * Snm;
    end
    
    % now do the final summation
    f = TA * cosmlam + TB * sinmlam;
end



