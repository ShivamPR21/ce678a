%% EXAMPLE for the use of GSHSAG/GSHS_GRID - 2015-05-22
clear all;
close all;
clc;

addpath('..');              % path to "SHbundle"
addpath('../../uberall');   % path to "uberall"

constants; % load constants from UBERALL/constants

% parse ICGEM-file up to max_lm = 10
[gsm, lmax, lmin, info] = parse_icgem('./data/example.icgem', 'max_lm', 10); 
% convert l,m-indexed list into /S|C\-format
[field_sc, lmax]        = clm2sc(gsm, 'max_lm', lmax); 

% prepare a longitude/latitude grid
lam = -179:1:180; lRAD = lam * pi / 180;
phi = -90:1:90;   pRAD = phi * pi / 180;
[Lam, Phi] = meshgrid(lam, phi);

%% calculation
% replace deprecated function...
tic; 
f  = gshsag(field_sc, lRAD, pRAD, 0, lmax, 'potential', 1, 'constants', 0);
fc = gshsag(field_sc, lRAD, pRAD, 0, lmax, 'potential', 1, 'constants', 1);
toc;

% ... by the new speed optimized function... 
% (however, for max_lm = 10, you can't see a difference ...)
tic; 
F   = gshs_grid(field_sc, lRAD, pRAD, ae, 'GM', GM, 'height', 0, 'max_lm', lmax, 'quant', 'potential', 'sub_wgs84', true, 'curvature', false, 'waitbar', true, 'legendre', 'mex'); 
Fc  = gshs_grid(field_sc, lRAD, pRAD, ae, 'GM', GM, 'height', 0, 'max_lm', lmax, 'quant', 'potential', 'sub_wgs84', true, 'curvature', true, 'waitbar', true, 'legendre', 'mex');
toc;

%% output
figure;
subplot(2, 1, 1);
pcolor(lam, phi, f); shading flat; 
xlabel('longitude'); ylabel('latitude'); title('GSHSAG: GRS80 reduced potential, [m^2/s^2]'); 
axis equal;
subplot(2, 1, 2);
pcolor(Lam, Phi, fc); shading flat; 
xlabel('longitude'); ylabel('latitude'); title('curvature of GRS80 reduced potential'); 
axis equal;

figure;
subplot(2, 1, 1);
pcolor(lam, phi, F); shading flat; 
xlabel('longitude'); ylabel('latitude'); title('GSHS\_GRID: GRS80 reduced potential, [m^2/s^2]'); 
axis equal;
subplot(2, 1, 2);
pcolor(Lam, Phi, Fc); shading flat; 
xlabel('longitude'); ylabel('latitude'); title('curvature of GRS80 reduced potential'); 
axis equal;