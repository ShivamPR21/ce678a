%% EXAMPLE for the use of GSHSPTW/GSHS_PTW - 2015-09-02
clear all;
close all;
clc;

% Don't forget to adjust the paths to SHbundle and uberall bundle! 
addpath('..');              % path to "SHbundle"
addpath('../../uberall');   % path to "uberall"

constants; % load constants from UBERALL/constants

[gsm, lmax, lmin, info] = parse_icgem('./data/example.icgem', 'max_lm', 10); % parse ICGEM-file up to max_lm = 10
[field, lmax]           = clm2sc(gsm, 'max_lm', lmax); % convert l,m-indexed list into /S|C\-format

len = 100000;
lamRAD = linspace(0, 90, len) * pi / 180; % define coordinates
phiRAD = linspace(0, 0, len) * pi / 180;
r      = ones(len, 1) * 7e6;

% replace deprecated function ...
tic; f = gshsptw(field, lamRAD, phiRAD, r, lmax, 'potential'); toc; % calculate potential for points (lamRAD, phiRAD)
% ... by the new speed optimized function ...
tic; F = gshs_ptw(field, lamRAD, phiRAD, r, ae, 'GM', GM, 'max_lm', lmax, 'quant', 'potential', 'waitbar', false); toc; % calculate potential for points (lamRAD, phiRAD)

figure;
plot(f); 
xlabel('point'); ylabel('[m^2/s^2]'); title('WGS84 reduced potential'); % output

figure;
plot(F); 
xlabel('point'); ylabel('[m^2/s^2]'); title('WGS84 reduced potential'); % output
