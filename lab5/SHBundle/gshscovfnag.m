function covfn = gshscovfnag(field,lmax,loc,quant,grid,fil,h,ivartype)

% GSHSCOVFNAG computes the covariance function in the spatial domain for a
% given point in space. This function is the same as GSHSCOVFN but can
% compute the covariance function for an irregular grid as well. When the
% grid is regular, the speed is the same with the GSHSCOVFN.
%
% [fvar,theta,lam] = gshscovfnag(field,lmax,loc,quant,grid,fil,h,ivartype)
%
% IN:
%    field .... A [(L+1)^2 * (L+1)^2] variance-covariance matrix arranged
%               in Colombo ordering, or, a structure with two variables:
%               1. a character array with the covariance matrix file;
%               2. path where the file is stored;
%               or, a character array of the filename and the path
%               together,
%               or, if only the variances are available then provide them
%               in either SC, CS, or Colombo ordering formats, and provide
%               an empty matrix for ivartype.
%               Use the first option only when the maximum degree of the
%               spherical harmonic expansion is [lmax <= 70], and also
%               provide the complete covariance matrix.
%    lmax ..... Maximum degree of the spherical harmonic expansion.
%    loc ...... Co-ordinates of the point for which the covariance function
%               is sought. It must be of the form [90-lat,lam] (radians)
%                                                          -def: [pi,pi]/4
%    quant .... optional string argument, defining the field quantity:
%               'none' (coefficients define the output), 'geoid',
%               'potential', 'dg' or 'gravity' (grav. anomaly), 'tr' (grav.
%               disturbance), or 'trr' (2nd rad. derivative), 'water'
%               (water equivalent heights), 'smd' (surface mass density).
%               If in the case of cross-covariance computation quant is a
%               cell array with two-variables. One for each field.
%                                                           -def: 'none'
%    grid ..... co-ordinates of the points where the covariances with
%               respect to the 'loc' have to be computed. The grid must be
%               given as a list of points [co-latitude longitude] in
%               radians.
%    fil ...... filter types that are used for spatial averaging. Input is
%               a structure variable with the name and the required
%               parameters of the filters.
%               options:
%               1. 'gauss'  - Gaussian smoothing. Parameter for the filter
%               is a smoothing radius. 200 <= r [km]
%               2. 'pell'   - Pellinen smoothing. Parameter for the filter
%               is a spherical cap angle psi. 0 < psi < 90 [degrees]
%               3. 'han'    - Non-isotropic smoothing proposed by Shin-Chan
%               Han et al., 2005, GJI 163, 18--25. Parameters for the
%               filter are
%                       m1  - order threshold [no units]
%                       r0  - fundamental radius [km]
%                       r1  - secondary radius [km]
%               4. 'other'   -  Other filter co-efficients can either be 
%               given as a [l Wl] vector (isotropic case) or as a matrix 
%               in CS- / SC- / [l m Clm Slm] in colombo ordering formats
%               (anisotropic case).
%               5. 'hann'   - Hanning smoothing. Parameter is smoothing
%               radius r [km]. The radius defined here is different from
%               Gaussian, because at the radius specified the filter
%               co-efficients become zero. Whereas in Gaussian smoothing
%               at the radius the filter coefficient value becomes half.
%               6. 'none'   - no filtering will be applied
%                                                           - def: 'none'
%    h ........ height for upward continuation [m]. It is also possible to
%               provide to different heights if the covariance matrix is
%               between two fields that are representative of two different
%               heights.
%                                                           - def: 0.
%    ivartype.. type of variance-covariance propagation
%               1. 'full' - full spectral variance-covariance matrix is
%                           propagated.
%               2. 'block'- block-diagonal variance-covariance matrix is
%                           propagated.
%               3. 'diag' - only the diagonal of the variance-covariance
%                           matrix is propagated.
%                                                           - def: 'diag'
%
% OUT:
%    fvar ..... covariance function for the grid points provided
%
% EXAMPLE:
%    field   	= rand(46);
%    lmax 		= 45;
%    loc 		= [pi/2 pi/5];
%    grid 		= [0,pi/3; pi/9, pi/8; pi/1.5, 1.5*pi];
%    n 		    = struct('size',pi/180, 'length',30*pi/180);
%    fil 		= struct('fil','han','m1',5,'r0',1e3,'r1',2e3)
%    h 		    = 0;
%    ivartype 	= 'cs';
%
%    [f,th,lam] = gshscovfnag(field,lmax,loc,[],grid,fil,h,ivartype)
%
% USES:
%    CS2SC, GRULE, eigengrav, PLM, COVORD, clm2sc, sc2cs
%    PELLINEN, GAUSSIAN, HANNONISO, VONHANN, CSSC2CLM
%
% SEE ALSO:
%    gshs, gsha, gshscov, gshscpxcovfn

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Balaji DEVARAJU (BD), GI, Uni Stuttgart
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2014-09-25: MR, replace calls of gcoef2sc by clm2sc
%    2008-08-21: BD, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse GSHS2PTFUN instead. \n--> Please check the help of the new function and update your code!');

tic
fprintf('----------------------------------------------------------------------\n')
fprintf('\t Computing spatial covariances/bi-polar field values \n')
fprintf('----------------------------------------------------------------------\n')

fprintf('Checking input arguments ... ')
%-----------------------------------------------------
% Checking the input and augmenting initial values
%-----------------------------------------------------
if nargin < 2, error('Insufficient input arguments'),       end
if nargin > 8, error('Too many input arguments!'),          end
if nargin <= 7,                      ivartype = 2;            end
if nargin <= 6 || isempty(h),        h        = 0;             end
if nargin <= 5 || isempty(fil),      fil      = struct('type','none'); end
if nargin <= 4 || isempty(grid),     grid     = 'mesh';       end
if nargin <= 3 || isempty(quant),    quant    = 'none';       end
if nargin <= 2 || isempty(loc),      loc      = [pi,pi]/4;      end

%-------------------------
% Checking field types
%-------------------------
if isstruct(field)
    fn          = fieldnames(field);
    fname       = field.(fn{1});
    pth         = field.(fn{2});
    delfile     = 0;
elseif ischar(field)
    fname       = field;
    pth         = pwd;
    pth         = [pth,'/'];
    delfile     = 0;
elseif isempty(ivartype)
    ivartype = 'diag';
    [rows,cols] = size(field);
    if isequal(cols,4) && isequal(rows, sum(1:lmax+1))
        field = sc2cs(clm2sc(field));
    elseif isequal(cols,(2*lmax+1))
        field = sc2cs(field);
    elseif ~isequal(rows,cols)
        error('Error matrix not in the SC, CS, or Colombo ordering formats')
    end
    vec = cssc2clm(field,lmax);
    vec = sparse(diag([vec(:,3); vec(lmax+2:end,4)]));
    save 'qmatnow.mat' vec
    % vec         = []; %Hmoh
    fname       = 'qmatnow.mat';
    pth         = pwd;
    pth         = [pth,'/'];
    delfile     = 1;
else
    [rows,cols] = size(field);
    if ~isequal(rows,cols) || ~isequal((lmax+1)^2,rows)
        error('Matrix dimensions do not agree with the maximum degree of the spherical harmonic expansion')
    end
    save 'qmatnow.mat' field
    fname       = 'qmatnow.mat';
    pth         = pwd;
    pth         = [pth,'/'];
    delfile     = 1;
end

%----------------------------------------------------
% Checking required input and output matrix types
%----------------------------------------------------
if nargin == 8
    if strcmp(ivartype,'full')
        ivartype = 0;
    elseif strcmp(ivartype,'block')
        ivartype = 1;
    elseif strcmp(ivartype,'diag')
        ivartype = 2;
    else
        display('Warning: Unknown covariance matrix structure')
    end
end

fprintf('done\t')
toc

tic
% ------------------------
% Grid definition.
% ------------------------
if size(grid,2) < 2
    error('Longitude values are missing in the GRID variable')
end
theta = grid(:,1);
grid  = grid((theta>=0 & theta<=pi),:);
theta = unique(grid(:,1));
lam   = grid(:,2);
if lam<0 || lam>(2*pi)
    lam(lam<0) = lam(lam<0) + 2*pi;
    lam(lam>(2*pi)) = lam(lam>2*pi) - 2*pi;
end
grid(:,2) = lam;
nlat = length(theta);

fprintf('Number of latitude points: %f \n',nlat)

fprintf('Preparing for spherical harmonic synthesis ... ')
%------------------------------------
% Preprocessing on the coefficients:
%    - specific transfer (quant)
%    - filtering (fil,cap)
%    - upward continuation (h)
% -----------------------------------

l      = (0:lmax)';
transf = eigengrav(l,quant,h);

%-----------------------------------
% Processing filter (coefficients)
%-----------------------------------
fldvar = fieldnames(fil);
if strcmp(fil.(fldvar{1}),'gauss')
    filcff = gaussian(lmax,fil.(fldvar{2}));
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'pell')
    filcff = pellinen(l,fil.(fldvar{2}));
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'han')
    m1      = fil.(fldvar{2});
    r0      = fil.(fldvar{3});
    r1      = fil.(fldvar{4});
    filcff = sc2cs(clm2sc(hannoniso(lmax,m1,r0,r1)));
    filcff = filcff.*sc2cs(clm2sc(cssc2clm([(0:lmax)' transf],lmax)));
elseif strcmp(fil.(fldvar{1}),'other')
    filcff = fil.(fldvar{2});
    filcff = sc2cs(clm2sc(cssc2clm(filcff,lmax)));
    filcff = filcff.*sc2cs(clm2sc(cssc2clm([(0:lmax)' transf],lmax)));
elseif strcmp(fil.(fldvar{1}),'hann')
    filcff = vonhann(lmax,fil.(fldvar{2}));
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'none')
    filcff = transf;
end

fprintf('done \t')
toc

tic
fprintf('Starting spherical harmonic synthesis ')
%-------------------------------------------------------------
% Step 1 consists of calculating the cc cs sc ss coefficients
%-------------------------------------------------------------
cols = lmax + 1;

% If N > L this will appear as a 2D zero-padding as we are dealing with 2D
% Fourier transforms
cc = zeros(cols);
cs = zeros(cols);
sc = zeros(cols);
ss = zeros(cols);

% Initializing variance matrix
covfn 		 = zeros(size(grid,1),3);
covfn(:,1:2) = grid;

% Computing covariance functions
if ivartype==0
	fprintf('of fully populated matrix\n')
    fprintf('Reading fully populated matrix ... ')
    covcell = covord(fname,pth,lmax,0);
    fprintf('done \t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for k = 1:lmax+1
            for m = k:lmax+1
                temp = (filcff(m:end)*filcff(k:end)');
                covcell.cc{m,k} = temp.*covcell.cc{m,k};
                covcell.cs{m,k} = temp.*covcell.cs{m,k};
                covcell.ss{m,k} = temp.*covcell.ss{m,k};
                covcell.sc{m,k} = temp.*covcell.sc{m,k};
            end
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for k = 1:lmax+1
            for m = k:lmax+1
                temp = (filcff(m:end,m)*filcff(k:end,k)');
                covcell.cc{m,k} = temp.*covcell.cc{m,k};
                covcell.cs{m,k} = temp.*covcell.cs{m,k};
                covcell.ss{m,k} = temp.*covcell.ss{m,k};
                covcell.sc{m,k} = temp.*covcell.sc{m,k};
            end
        end
    end
    for i = 1:nlat
        for k = 0:lmax
            for m = k:lmax
                pnm = plm(m:lmax,m,loc(1));
                plk = plm(k:lmax,k,theta(i));
                temp = pnm'*plk;
                cc(m+1,k+1) = sum(sum(temp.* covcell.cc{m+1,k+1}));
                cs(m+1,k+1) = sum(sum(temp.* covcell.cs{m+1,k+1}));
                sc(m+1,k+1) = sum(sum(temp.* covcell.sc{m+1,k+1}));
                ss(m+1,k+1) = sum(sum(temp.* covcell.ss{m+1,k+1}));

                if m~=k
                    pnm = plm(m:lmax,m,theta(i));
                    plk = plm(k:lmax,k,loc(1));
                    temp = plk'*pnm;
                    cc(k+1,m+1) = sum(sum(temp.*covcell.cc{k+1,m+1}));
                    cs(k+1,m+1) = sum(sum(temp.*covcell.cs{k+1,m+1}));
                    sc(k+1,m+1) = sum(sum(temp.*covcell.sc{k+1,m+1}));
                    ss(k+1,m+1) = sum(sum(temp.*covcell.ss{k+1,m+1}));
                end
            end
        end
        ind = (covfn(:,1)==theta(i));
        lam = covfn(ind,2);
        mlamloc = loc(2)*(0:cols-1);
        mlam 	= lam*(0:cols-1);
        f = cos(mlamloc)*cc*cos(mlam');
        f = f + sin(mlamloc)*sc*cos(mlam');
        f = f + cos(mlamloc)*cs*sin(mlam');
        f = f + sin(mlamloc)*ss*sin(mlam');
        covfn(ind,3) = f;
        cc = zeros(cols);
        cs = zeros(cols);
        sc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
elseif ivartype==1    
    fprintf('of block-diagonal matrix\n')
    fprintf('Reading block-diagonal matrix ... ')
    covcell = covord(fname,pth,lmax,1);
    fprintf('done\t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for m = 1:lmax+1
            temp = filcff(m:end);
            temp = temp*temp';
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for m = 1:lmax+1
            temp = filcff(m:end,m);
            temp = temp*temp';
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    end
    for i = 1:nlat
        for m = 0:lmax
            pnm = plm(m:lmax,m,loc(1));
            plk = plm(m:lmax,m,theta(i));
            temp = pnm'*plk;
            cc(m+1,m+1) = sum(sum(temp.* covcell.cc{m+1}));
            ss(m+1,m+1) = sum(sum(temp.* covcell.ss{m+1}));
        end
        ind = (covfn(:,1)==theta(i));
        lam = covfn(ind,2);
        mlamloc = loc(2)*(0:cols-1);
        mlam 	= lam*(0:cols-1);
        f = cos(mlamloc)*cc*cos(mlam');
        f = f + sin(mlamloc)*sc*cos(mlam');
        f = f + cos(mlamloc)*cs*sin(mlam');
        f = f + sin(mlamloc)*ss*sin(mlam');
        covfn(ind,3) = f;
        cc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
elseif ivartype == 2
    fprintf('of diagonal matrix\n')
    fprintf('Reading diagonal matrix ... ')
    covcell = covord(fname,pth,lmax,2);
    fprintf('done\t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for m = 1:lmax+1
            temp = filcff(m:end).^2;
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for m = 1:lmax+1
            temp = filcff(m:end,m).^2;
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    end
    for i = 1:nlat
        for m = 0:lmax
            pnm = plm(m:lmax,m,loc(1));
            plk = plm(m:lmax,m,theta(i));
            temp = (pnm.*plk)';
            cc(m+1,m+1) = sum(temp.*covcell.cc{m+1});
            ss(m+1,m+1) = sum(temp.*covcell.ss{m+1});
        end
        ind = (covfn(:,1)==theta(i));
        lam = covfn(ind,2);
        mlamloc = loc(2)*(0:cols-1);
        mlam 	= lam*(0:cols-1);
        f = cos(mlamloc)*cc*cos(mlam');
        f = f + sin(mlamloc)*sc*cos(mlam');
        f = f + cos(mlamloc)*cs*sin(mlam');
        f = f + sin(mlamloc)*ss*sin(mlam');
        covfn(ind,3) = f;
        cc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
end
close(hwb);

fprintf('Covariance propagation complete\t')
toc
