PROGRAM All_EPS_to_PDF; {$M 2048, 0, 0}

USES sysutils, crt;

VAR Dir         : tSearchRec;
    FilName, Pfad  : AnsiString;
    Count       : LongInt;
    Com         : AnsiString;
    OSE         : Integer;
    newer       : Boolean;
    i           : Word;

BEGIN
  newer := TRUE;

  IF ParamCount = 0 THEN
    Pfad := '.\'
  ELSE
    FOR i := 1 TO ParamCount DO
    BEGIN
      IF (Copy(ParamStr(i), 1, 2) = '-?') OR
         (Copy(ParamStr(i), 1, 2) = '-h') THEN
      BEGIN
        WriteLn('allEPS2pdf - version 2009-05-06');
        WriteLn('USAGE: allEPS2PDF [-d=<dir> -a]');
        WriteLn;
        WriteLn('-d=<dir> ... output directory');
        WriteLn('-a ......... all files will be converted');
        WriteLn('             (if not stated, only new files will be converted)');
        Halt;
      END;

      IF Copy(ParamStr(i), 1, 2) = '-d' THEN
      BEGIN
        Pfad := Copy(ParamStr(i), 4, length(ParamStr(i)));
        IF Pfad[Length(Pfad)] <> '\' THEN Pfad := Pfad + '\';
        WriteLn('- output path: ', Pfad);
        CONTINUE;
      END;

      IF Copy(ParamStr(i), 1, 2) = '-a' THEN
      BEGIN
        newer := FALSE;
        CONTINUE;
      END;
    END;

  CASE newer OF
    FALSE : WriteLn('- all files will be converted...');
    TRUE  : WriteLn('- only new files will be converted...');
  END;

  Count := 0;
  IF FindFirst('*.eps', faAnyFile, Dir) = 0 THEN
  BEGIN
    REPEAT
      FilName := Copy(Dir.Name, 1, Length(Dir.Name) - 3) + 'pdf';

      IF newer AND FileExists(FilName) THEN
      BEGIN
        IF FileAge(FilName) >= FileAge(Dir.Name) THEN
        BEGIN
          WriteLn('skip (eps not changed):  ', Dir.Name);
          CONTINUE;
        END;
      END;

      WriteLn(Dir.Name + ' --> ' + Pfad + FilName);

      Com := '-dEPSCrop ' + Dir.Name + ' ' + Pfad + FilName + #0;
      OSE := ExecuteProcess('ps2pdf.exe', Com);
      IF OSE <> 0 then
      BEGIN
        WriteLn('ERROR OCCURED: ', OSE);
        BREAK;
      END;

      Inc(Count);
    UNTIL FindNext(Dir) <> 0;
  END;
  FindClose(Dir);

  Writeln ('Finished conversion. ', Count, ' files done.');
END.
