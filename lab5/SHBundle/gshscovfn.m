function [covfn,varargout] = gshscovfn(field,lmax,loc,quant,grd,n,fil,h,ivartype)

% GSHSCOVFN computes the covariance function in the spatial domain for a
% given point in space. This function is the same as GSHSCOV, but this
% computes the covariance function of a point instead of the variances in
% the spatial domain. This function can also be used for transforming the
% spectra of filter kernels.
%
% [covfn,theta,lam] = gshscovfn(field,lmax,loc,quant,grd,n,fil,h,ivartype)
% covfn = gshscovfn(field,lmax,loc,quant,'points',n,fil,h,ivartype)
%
% IN:
%    field .... A [(L+1)^2 * (L+1)^2] variance-covariance matrix arranged
%               in Colombo ordering, or, a structure with two variables:
%               1. a character array with the covariance matrix file;
%               2. path where the file is stored (this order must be
% 				maintained at all times);
%               or, a character array of the filename and the path
%               together,
%               or, if only the variances are available then provide them
%               in either SC, CS, or Colombo ordering formats.
%               Use the first option only when the maximum degree of the
%               spherical harmonic expansion is [lmax <= 70], and also
%               provide the complete covariance matrix.
%    lmax ..... Maximum degree of the spherical harmonic expansion.
%    loc ...... Co-ordinates of the point for which the covariance function
%               is sought. It must be of the form [90-lat,lamRAD] (radians)
%                                                          -def: [pi,pi]/4
%    quant .... optional string argument, defining the field quantity:
%               'none' (coefficients define the output), 'geoid',
%               'potential', 'dg' or 'gravity' (grav. anomaly), 'tr' (grav.
%               disturbance), or 'trr' (2nd rad. derivative), 'water'
%               (water equivalent heights), 'smd' (surface mass density).
%               If in the case of cross-covariance computation quant is a
%               cell array with two-variables. One for each field.
%                                                           -def: 'none'
%    grd ...... optional string argument, defining the grid:
%               1. 'pole' or 'mesh': equi-angular (N+1)*2N, including poles
%               and Greenwich meridian.                     -def: 'pole'
%               2. 'block' or 'cell': equi-angular block mid-points.
%               3. 'neumann' or 'gauss': Gauss-grid (N+1)*2N. If you use
%               'gauss' the settings for 'length' settings in 'n' are
%               overridden and only a global map is provided.
%               4. 'points': If the value of the covariance function is 
%               required only a few select then this option can be used.
% 				Longitude is always counted from [0,2*pi]
%    n ........ grid size parameter N. This parameter is a structure with
%               two entries. The first entry should provide the grid size, 
%               and the second entry should provide the extent in degrees 
%               on either side of latitude and longitude of the point 
%               whose covariance function is sought. e.g.
%               struct('size',pi/180,'length',[pi/9,pi/6,pi/4,pi/3])
%               tells us that the grid size is 1x1 (degree) grid, and the
%               covariance function must be calculated for loc from 20
%               degrees to the north of the location, 30 degrees to the
%               east, 45 degrees to the south, and 60 degrees to the west
%               of the location. The vector reads as [N E S W]. 
%               If you provide an empty matrix for n then default values 
%               will be used. size = 1 degree; length = 20 degrees. If for 
%               length parameter only one value is given then the same 
%               length is used along both latitude and longitude. If you 
%               want the entire global map you can specify the 'length' 
%               as 'global', and accordingly a grid is created. 
%               If the chosen grid type is 'points' then a two-column vector
%               must be provided for the points where the values are sought.
% 				----------------------------------------------------------
% 						NOTE: size (radians), length (radians)
% 				----------------------------------------------------------
%    fil ...... filter types that are used for spatial averaging. Input is
%               a structure variable with the name and the required
%               parameters of the filters.
%               options:
%               1. 'gauss'  - Gaussian smoothing. Parameter for the filter
%               is a smoothing radius. [radians]
%               2. 'pell'   - Pellinen smoothing. Parameter for the filter
%               is a spherical cap angle psi. 0 < psi < pi [radians]
%               3. 'han'    - Non-isotropic smoothing proposed by Shin-Chan
%               Han et al., 2005, GJI 163, 18--25. Parameters for the
%               filter are
%                       m1  - order threshold [no units]
%                       r0  - fundamental radius [radians]
%                       r1  - secondary radius [radians]
%               4. 'other'   -  Other filter co-efficients can either be 
%               given as a [l Wl] vector (isotropic case) or as a matrix 
%               in CS- / SC- / [l m Clm Slm] in colombo ordering formats
%               (anisotropic case).
%               5. 'hann'   - Hanning smoothing. Parameter is smoothing
%               radius r [km]. The radius defined here is different from
%               Gaussian, because at the radius specified the filter
%               co-efficients become zero. Whereas in Gaussian smoothing
%               at the radius the filter coefficient value becomes half.
%               6. 'none'   - no filtering will be applied
%                                                           - def: 'none'
%    h ........ height for upward continuation [m]. It is also possible to
%               provide to different heights if the covariance matrix is
%               between two fields that are representative of two different
%               heights.
%                                                           - def: 0.
%    ivartype.. type of variance-covariance propagation
%               1. 'full' - full spectral variance-covariance matrix is
%                           propagated.
%               2. 'block'- block-diagonal variance-covariance matrix is
%                           propagated.
%               3. 'diag' - only the diagonal of the variance-covariance
%                           matrix is propagated.
% 	         	4. 'cs'	  - if input is in CS/SC/[l m Clm Slm] formats.	
%                                                           - def: 'diag'
%
% OUT:
%    fvar .... covariance function for the region requested
%    thetaRAD ... vector of spherical distances from the center of the
%              covariance function [rad]
%    lamRAD ..... vector of azimuths [rad]
%
% EXAMPLE:
%    field  	= rand(46);
%    lmax   	= 45;
%    loc	    = [pi/2 pi/5];
%    grd 	    = 'pole';
%    n 		    = struct('size',pi/180, 'length',30*pi/180);
%    fil 	    = struct('fil','han','m1',5,'r0',1e3,'r1',2e3)
%    h 		    = 0;
%    ivartype 	= 'cs';
%
%    [f,th,lam] = gshscovfn(field,lmax,loc,[],grd,n,fil,h,ivartype)
%
% USES:
%    CS2SC, eigengrav, PLM, COVORD, clm2sc, sc2cs, PELLINEN, gaussfltr,
%    HANNONISO, VONHANN, CSSC2CLM, uberall\standing, uberall\GRULE
%
% SEE ALSO:
%    gshs, gsha, gshscov

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    Balaji DEVARAJU (BD), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2014-09-25: MR, replace calls of gcoef2sc by clm2sc
%    2008-08-21: BD, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse GSHS2PTFUN instead. \n--> Please check the help of the new function and update your code!');

tic
fprintf('----------------------------------------------------------------------\n')
fprintf('\t Computing spatial covariance/bi-polar functions \n')
fprintf('----------------------------------------------------------------------\n')

fprintf('Checking input arguments ... ')
%-----------------------------------------------------
% Checking the input and augmenting initial values
%-----------------------------------------------------
if nargin > 9, error('Too many input arguments!'),          end
if nargin < 9,                       ivartype = 2;            end
if nargin < 8 || isempty(h),        h        = 0;             end
if nargin < 7 || isempty(fil),      fil      = struct('type','none'); end
if nargin < 6 || isempty(n),        n        = struct('size',1,'length',[pi,pi]/9); end
if nargin < 5 || isempty(grd),     grd     = 'mesh';       end
if nargin < 4 || isempty(quant),    quant    = 'none';       end
if nargin < 3 || isempty(loc),      loc     = [pi,pi]/4;      end
if nargin < 2, error('Insufficient input arguments'),       end

if ischar(grd), grd  = lower(grd);    end

%-------------------------
% Checking field types
%-------------------------
if isstruct(field)
    fn          = fieldnames(field);
    fname       = field.(fn{1});
    pth         = field.(fn{2});
    delfile     = 0;
elseif ischar(field)
    fname       = field;
    pth         = [];
    delfile     = 0;
elseif strcmp(ivartype,'cs')
    ivartype    = 'diag';
    [rows,cols] = size(field);
    if isequal(cols,4) && isequal(rows, sum(1:lmax+1))
        field = sc2cs(clm2sc(field));
    elseif isequal(cols,(2*lmax+1))
        field = sc2cs(field);
    elseif ~isequal(rows,cols)
        error('Error matrix not in the SC, CS, or Colombo ordering formats')
    end
    vec = cssc2clm(field,lmax);
    vec = sparse(diag([vec(:,3); vec(lmax+2:end,4)]));
    save 'qmatnow.mat' vec
    % vec         = []; 
    fname       = 'qmatnow.mat';
    pth         = pwd;
    pth         = [pth,'/'];
    delfile     = 1;
else
    [rows,cols] = size(field);
    if ~isequal(rows,cols) || ~isequal((lmax+1)^2,rows)
        error('Matrix dimensions do not agree with the maximum degree of the spherical harmonic expansion')
    end
    save 'qmatnow.mat' field
    fname       = 'qmatnow.mat';
    pth         = pwd;
    pth         = [pth,'/'];
    delfile     = 1;
end

%----------------------------------------------------
% Checking required input and output matrix types
%----------------------------------------------------
if nargin == 9
    if strcmp(ivartype,'full')
        ivartype = 0;
    elseif strcmp(ivartype,'block')
        ivartype = 1;
    elseif strcmp(ivartype,'diag')
        ivartype = 2;
    else
        display('Warning: Unknown covariance matrix structure')
    end
end

fprintf('done\t')
toc

tic
fprintf('Preparing for spherical harmonic synthesis ... ')
% ------------------------
% Grid definition.
% ------------------------

if ~strcmp(grd,'points') && isstruct(n)
    if isempty(n.size)
        dt = pi/180;
    elseif n.size < 0
        error('n must be positive')
    else
        dt = n.size;
    end
    
    if isempty(n.length)
        n.length = [20,20,20,20];
    elseif length(n.length) == 1
        n.length = ones(4,1)*n.length;
    elseif length(n.length) == 2
        n.length = (n.length(:))';
        n.length = [n.length n.length];
    elseif length(n.length) == 3
        n.length = (n.length(:))';
        n.length = [n.length n.length(3)];
    elseif strcmp(n.length,'global')
        n.length = [loc(1)-0, (2*pi)-loc(2), pi-loc(1), loc(2)-0];
    end
    
    if strcmp(grd,'pole') || strcmp(grd,'mesh')
        theta = (loc(1)-(n.length(1)):dt:loc(1)+(n.length(3)))';
        lam   = (loc(2)-(n.length(4)):dt:loc(2)+(n.length(2)));
    elseif strcmp(grd,'block') || strcmp(grd,'cell')
        theta = ((loc(1)-(n.length(1))+dt/2):dt:(loc(1)+(n.length(3))))';
        lam   = ((loc(2)-(n.length(4))+dt/2):dt:(loc(2)+(n.length(2))));
        loc   = loc + dt/2;
    elseif strcmp(grd,'neumann') || strcmp(grd,'gauss')
        gx    = grule((pi/dt)+1);
        theta = flipud(acos(standing(gx)));
        lam   = 0:dt:(2*pi - dt);
    end
elseif strcmp(grd,'points') && ~isstruct(n)
    theta   = unique(n(:,1));
    lam     = n(:,2)';
else
    error('Unknown grid type')
end


% The following steps are mainly for pre-cautionary purposes, and to make sure
% that the co-latitude and longitude values remain within expected bounds.
thetaRAD = thetaRAD(thetaRAD>=0 & thetaRAD<=pi);
if lamRAD<0 || lamRAD>(2*pi)
    lamRAD(lamRAD<0) = lamRAD(lamRAD<0) + 2*pi;
    lamRAD(lamRAD>(2*pi)) = lamRAD(lamRAD>2*pi) - 2*pi;
end

%------------------------------------
% Preprocessing on the coefficients:
%    - specific transfer (quant)
%    - filtering (fil,cap)
%    - upward continuation (h)
% -----------------------------------

l    	= (0:lmax)';
transf 	= eigengrav(l,quant,h);

%-----------------------------------
% Processing filter (coefficients)
%-----------------------------------
fldvar = fieldnames(fil);
if strcmp(fil.(fldvar{1}),'gauss')
    filcff = gaussfltr(fil.(fldvar{2}),lmax);
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'pell')
    filcff = pellinen(l,fil.(fldvar{2}));
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'han')
    m1      = fil.(fldvar{2});
    r0      = fil.(fldvar{3});
    r1      = fil.(fldvar{4});
    filcff = sc2cs(clm2sc(hannoniso(r0,m1,r1,lmax)));
    filcff = filcff.*sc2cs(clm2sc(cssc2clm([(0:lmax)' transf],lmax)));
elseif strcmp(fil.(fldvar{1}),'other')
    filcff = fil.(fldvar{2});
    filcff = sc2cs(clm2sc(cssc2clm(filcff,lmax)));
    filcff = filcff.*sc2cs(clm2sc(cssc2clm([(0:lmax)' transf],lmax)));
elseif strcmp(fil.(fldvar{1}),'hann')
    filcff = vonhann(fil.(fldvar{2}),lmax);
    filcff = filcff.*transf;
elseif strcmp(fil.(fldvar{1}),'none')
    filcff = transf;
end

fprintf('done \t')
toc

tic
fprintf('Starting spherical harmonic synthesis ')
%-------------------------------------------------------------
% Step 1 consists of calculating the cc cs sc ss coefficients
%-------------------------------------------------------------
cols = lmax + 1;

cc = zeros(cols);
cs = zeros(cols);
sc = zeros(cols);
ss = zeros(cols);

% Initializing variance matrix
nlat    = length(theta);
if strcmp(grd,'points')
    covfn 		 = zeros(size(n,1),3);
    covfn(:,1:2) = n;
else
    nlon    = length(lam);
    covfn   = zeros(nlat,nlon);
end


% Computing covariance functions
if ivartype==0
	fprintf('of fully populated matrix\n')
    fprintf('Reading fully populated matrix ... ')
    covcell = covord(fname,pth,lmax,0);
    fprintf('done \t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for k = 1:lmax+1
            for m = k:lmax+1
                temp = (filcff(m:end)*filcff(k:end)');
                covcell.cc{m,k} = temp.*covcell.cc{m,k};
                covcell.cs{m,k} = temp.*covcell.cs{m,k};
                covcell.ss{m,k} = temp.*covcell.ss{m,k};
                covcell.sc{m,k} = temp.*covcell.sc{m,k};
            end
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for k = 1:lmax+1
            for m = k:lmax+1
                temp = (filcff(m:end,m)*filcff(k:end,k)');
                covcell.cc{m,k} = temp.*covcell.cc{m,k};
                covcell.cs{m,k} = temp.*covcell.cs{m,k};
                covcell.ss{m,k} = temp.*covcell.ss{m,k};
                covcell.sc{m,k} = temp.*covcell.sc{m,k};
            end
        end
    end
    for i = 1:nlat
        for k = 0:lmax
            for m = k:lmax
                pnm = plm(m:lmax,m,loc(1));
                plk = plm(k:lmax,k,thetaRAD(i));
                temp = pnm'*plk;
                cc(m+1,k+1) = sum(sum(temp.* covcell.cc{m+1,k+1}));
                cs(m+1,k+1) = sum(sum(temp.* covcell.cs{m+1,k+1}));
                sc(m+1,k+1) = sum(sum(temp.* covcell.sc{m+1,k+1}));
                ss(m+1,k+1) = sum(sum(temp.* covcell.ss{m+1,k+1}));

                if m~=k
                    plk = plm(k:lmax,k,loc(1));
                    pnm = plm(m:lmax,m,thetaRAD(i));
                    temp = plk'*pnm;
                    cc(k+1,m+1) = sum(sum(temp.*covcell.cc{k+1,m+1}));
                    cs(k+1,m+1) = sum(sum(temp.*covcell.cs{k+1,m+1}));
                    sc(k+1,m+1) = sum(sum(temp.*covcell.sc{k+1,m+1}));
                    ss(k+1,m+1) = sum(sum(temp.*covcell.ss{k+1,m+1}));
                end
            end
        end
        mlamloc = loc(2)*(0:cols-1);
        if strcmp(grd,'points')
            ind     = (covfn(:,1)==theta(i));
            mlam 	= (0:cols-1)'*lam(ind);

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*sc*cos(mlam);
            f = f + cos(mlamloc)*cs*sin(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);
            covfn(ind,3) = f;
        else
            mlam    = (0:cols-1)'*lam;

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*sc*cos(mlam);
            f = f + cos(mlamloc)*cs*sin(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);
            covfn(i,:) = f;
        end
        cc = zeros(cols);
        cs = zeros(cols);
        sc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
elseif ivartype==1
    
    fprintf('of block-diagonal matrix\n')
    fprintf('Reading block-diagonal matrix ... ')
    covcell = covord(fname,pth,lmax,1);
    fprintf('done\t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for m = 1:lmax+1
            temp = filcff(m:end);
            temp = temp*temp';
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for m = 1:lmax+1
            temp = filcff(m:end,m);
            temp = temp*temp';
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    end
    for i = 1:nlat
        for m = 0:lmax
            pnm = plm(m:lmax,m,loc(1));
            plk = plm(m:lmax,m,thetaRAD(i));
            temp = pnm'*plk;
            cc(m+1,m+1) = sum(sum(temp.* covcell.cc{m+1}));
            ss(m+1,m+1) = sum(sum(temp.* covcell.ss{m+1}));
        end
        mlamloc = loc(2)*(0:cols-1);
        if strcmp(grd,'points')
            ind     = (covfn(:,1)==theta(i));
            mlam    = (0:cols-1)'*lam(ind);

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);

            covfn(ind,3) = f;
        else
            mlam 	= (0:cols-1)'*lam;

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);

            covfn(i,:) = f;
        end
        cc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
elseif ivartype == 2
    
    fprintf('of diagonal matrix\n')
    fprintf('Reading diagonal matrix ... ')
    covcell = covord(fname,pth,lmax,2);
    fprintf('done\t')
    toc
    tic
    hwb    = waitbar(0,'Percentage of latitude points ready ...');
    set(hwb,'NumberTitle','off','Name','Bi-polar SH propagation')
    if delfile == 1, delete(fname), end

    if size(filcff,2) == 1
        for m = 1:lmax+1
            temp = filcff(m:end).^2;
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    elseif size(filcff,1)==size(filcff,2) && size(filcff,1)==lmax+1
        for m = 1:lmax+1
            temp = filcff(m:end,m).^2;
            covcell.cc{m} = temp.*covcell.cc{m};
            covcell.ss{m} = temp.*covcell.ss{m};
        end
    end
    for i = 1:nlat
        for m = 0:lmax
            pnm = plm(m:lmax,m,loc(1));
            plk = plm(m:lmax,m,thetaRAD(i));
            temp = (pnm.*plk)';
            cc(m+1,m+1) = sum(temp.*covcell.cc{m+1});
            ss(m+1,m+1) = sum(temp.*covcell.ss{m+1});
        end
        mlamloc = loc(2)*(0:cols-1);
        if strcmp(grd,'points')
            ind     = (covfn(:,1)==theta(i));
            mlam    = (0:cols-1)'*lam(ind);

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);

            covfn(ind,3) = f;
        else
            mlam 	= (0:cols-1)'*lam;

            f = cos(mlamloc)*cc*cos(mlam);
            f = f + sin(mlamloc)*ss*sin(mlam);

            covfn(i,:) = f;
        end
        cc = zeros(cols);
        ss = zeros(cols);
        waitbar(i/nlat)
    end
end
close(hwb);

if ~strcmp(grd,'points')
    varargout{1} = theta;
    varargout{2} = lam;
end

fprintf('Covariance propagation complete\t')
toc
