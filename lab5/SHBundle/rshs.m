function f = rshs(field,quant,theRAD,lamRAD,h,jflag)

% RSHS regional spherical harmonic synthesis 
%
% HOW:
%    f = rshs(field)
%    f = rshs(field,quant,theRAD,lamRAD,h,jflag)
%
% IN:
%    field ......... set of SH coefficients, either in SC-triangle or CS-square format 
%    quant ......... optional string argument, defining the field quantity:
%                    'none' (coefficients define the output), 'geoid', 'potential',
%                    'dg' or 'gravity' (grav. anomaly), 'tr' (grav. disturbance), or
%                    'trr' (2nd rad. derivative). 		        	- def: 'none'
%    theRAD ........ vector of co-latitudes [rad] 			        - 'pole'-grid with
%    lamRAD ........ vector of longitudes [rad]			            N = LMAX
%    h ............. height [m]     					            - def: 0.
%    jflag ......... determines whether GRS80 is subtracted.		- def: true (1)
%
% OUT:
%    f ............. the regional field
%
% EXAMPLE:
%    load egm96; lmax = 200;
%    field =  vec2cs(lmax,EGM96(:,3),EGM96(:,4))
%    theRAD = [0:.001:.5],lamRAD = [0:.007:.5]
%    [V_pot] = rshs(field,'potential', theRAD, lamRAD,0,1);
%    figure;
%    imagesc(lamRAD*180/pi,theRAD*180/pi, V_pot); title('potential')
%
% USES:
%    vec2cs, plm, cs2sc, eigengrav, normalklm, 
%    uberall/standing, uberall/checkcoor
% 
% SEE ALSO:
%    GSHSAG, GSHS GSHA

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Nico SNEEUW (NS), IAPG, TU-Munich
%    Matthias WEIGELT (MW), GI, Uni Stuttgart
%    Markus ANTONI (MA), GI, Uni Stuttgart 
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2018-11-27: MA, extra warning if a reference field is subtracted
%                    (request of external users)
%    2013-02-13: MR, change function names, brush up comments
%    2013-01-30: MA, comments/removing of smoothing option
%    2013-01-23: MA, input in radian
%    1994-??-??: NS, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse REGIONALSHS instead. \n--> Please check the help of the new function and update your code!');

% -------------------------------------------------------------------------
% Some checking, default settings and further preliminaries.
% A lot of checking is done by EIGENGRAV as well.
% -------------------------------------------------------------------------
[rows, cols] = size(field);
if rows == cols					% field is in CS-format
   lmax  = rows - 1;
   field = cs2sc(field);			 	% convert to SC-format
elseif cols-2*rows == -1				% field is in SC-format already
   lmax  = rows - 1;
else
   error('Check format of field.')
end

if nargin > 6, error('Too many input arguments!'), end
if nargin < 6, jflag = 1;      end
if nargin < 5, h     = 0;      end
if nargin < 4, lamRAD   = linspace(0,2*pi,2*lmax+1); lamRAD(2*lmax+1) = []; end
if nargin < 3, theRAD    = linspace(0,pi,lmax+1)'; end
if nargin < 2, quant = 'none'; end

% prepare the coordinates
[lamRAD, phiRAD, ~] = checkcoor(lamRAD, pi/2 - theRAD, 0, 0, 'grid');
theRAD = pi/2 - phiRAD;
lamRAD = lamRAD';

nlat = length(theRAD);
% nlon = length(lamRAD);

% -------------------------------------------------------------------------
% Preprocessing on the coefficients: 
%    - subtract reference field (if jflag is set)
%    - specific transfer
%    - upward continuation
% -------------------------------------------------------------------------
if jflag, field = field - cs2sc(normalklm(lmax)); 
    warning('A reference field (WGS84) is removed from your coefficients')
end
l = standing(0:lmax);
transf = eigengrav(l,quant,h);
field  = field .* (transf * ones(1,2*lmax+1));

% -------------------------------------------------------------------------
% Size declarations and start the waitbar:
% -------------------------------------------------------------------------
a      = zeros(nlat,lmax+1);
b      = zeros(nlat,lmax+1);
hwb    = waitbar(0,'Percentage of orders m ready ...');
set(hwb,'NumberTitle','off','Name','RSHS')

% -------------------------------------------------------------------------
% Treat m = 0 separately
% -------------------------------------------------------------------------
m = 0;
c = field(m+1:lmax+1,lmax+1+m);
p = plm(m:lmax,m,theRAD);  
a(:,m+1) = p * c;
b(:,m+1) = zeros(nlat,1);
waitbar((m+1)/(lmax+1)) 			% Update the waitbar

% -------------------------------------------------------------------------
% Do loop m = 1 to lmax
% -------------------------------------------------------------------------
for m = 1:lmax
   c = field(m+1:lmax+1,lmax+1+m);
   s = field(m+1:lmax+1,lmax+1-m);
   p = plm(m:lmax,m,theRAD); 
   a(:,m+1) = p * c;
   b(:,m+1) = p * s;
   waitbar((m+1)/(lmax+1)) 			% Update the waitbar
end

clear field
close(hwb)

% -------------------------------------------------------------------------
% The second synthesis step consists of an inverse Fourier transformation.
% Just multiply cos(m*lam) and sin(m*lam) here.
% -------------------------------------------------------------------------
m = 0:lmax;
c = cos(m'*lamRAD);
s = sin(m'*lamRAD);

f = a * c + b * s;
