function [f, theRAD, lamRAD] = gshs(field, quant, grd, n, h, jflag)

% GSHS global spherical harmonic synthesis 
% f = gshs(field)
%
% IN:
%    field ... set of SH coefficients, either in SC-triangle or CS-square format 
%    quant ... optional string argument, defining the field quantity:
%              - 'none' ............. (default), coefficients define the output
%              - 'geoid' ............ geoid height [m],
%              - 'potential' ........ potential [m^2/s^2],
%              - 'dg', 'gravity' .... gravity anomaly [mGal], 
%              - 'tr' ............... grav. disturbance, 1st rad. derivative [mGal],
%              - 'trr' .............. 2nd rad. derivative [1/s^2],
%              - 'water' ............ equiv. water height [m],
%              - 'smd' .............. surface mass density [kg/m^2]. 
%     grd .... optional string argument, defining the grid:
%              - 'pole', 'mesh' ..... (default), equi-angular (n+1)*2n, including 
%                                     poles and Greenwich meridian.
%              - 'block', 'cell' .... equi-angular block midpoints. n*2n
%              - 'neumann', 'gauss' . Gauss-grid (n+1)*2n
%     n ...... grid size parameter n. (default: n = lmax, determined from field)
%              #longitude samples: 2*n
%              #latitude samples n ('blocks') or n+1.
%     h ...... (default: 0), height above Earth mean radius [m].
%     jflag .. (default: true), determines whether to subtract GRS80.
%
% OUT:
%    f ....... the global field
%    theRAD .. vector of co-latitudes [rad] 
%    lamRAD .. vector of longitudes [rad]
%
% EXAMPLE: see SHBUNDLE/examples/example_gshs.m
%
% USES:
%    vec2cs, cs2sc, eigengrav, plm, normalklm, 
%    uberall/grule, uberall/standing, uberall/isint, uberall/ispec
% 
% SEE ALSO:
%    GSHSAG, RSHS, GSHA

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Nico SNEEUW (NS), IAPG, TU-Munich
%    Matthias WEIGELT (MW), DoGE, UofC  
%    Markus ANTONI (MA), GI, Uni Stuttgart 
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    Balaji DEVARAJU (BD), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2018-11-27: MA, extra warning if a reference field is subtracted
%                    (request of external users)
%    2017-09-23: BD, Replaced waitbar with twaitbar
%    2015-05-22: MR, declare deprecated, use the fork 'gshs_' instead
%    2014-03-09: BD, changed the variable 'grid' to 'grd' as 'grid' conflicts
%                    with the function 'grid'
%    2014-01-14: MR, brush up help text, beautify code, exchange deprecated
%                    'isstr' by 'ischar'
%    2013-02-13: MR, change function names, brush up comments
%    2013-01-30: MA, comments/removing of smoothing option
%    2013-01-23: MA, output in radian
%    2013-01-18: MA, replacement: isscal -> isscalar
%    1998-08-28: NS, brushing up and redefinition of checks
%    1994-??-??: NS, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', ['DEPRECATED!\n', ...
        'This function will be removed in a future version of SHbundle.\n', ...
        'Use GSHS_ instead. \n', ...
        '--> Please check the help to this function and update your code!']);

% -------------------------------------------------------------------------
% Some checking, default settings and further preliminaries.
% A lot of checking is done by EIGENGRAV as well.
% -------------------------------------------------------------------------
[rows,cols] = size(field);
if rows == cols					    % field is in CS-format
   lmax  = rows - 1;
   field = cs2sc(field);			% convert to SC-format
elseif cols-2*rows == -1			% field is in SC-format already
   lmax  = rows - 1;
else
   error('Check format of field.')
end

if nargin > 6, error('Too many input arguments!'), end
if nargin < 6, jflag = 1;      end
if nargin < 5, h     = 0;      end
if nargin < 4, n     = lmax;   end
if nargin < 3, grd   = 'mesh'; end
if nargin < 2, quant = 'none'; end

if ~(isscalar(n) && isint(n))
    error('n must be integer & scalar')
end
if ~ischar(grd)
    error('grid argument must be string')
end 
grd = lower(grd);

% -------------------------------------------------------------------------
% Grid definition.
% -------------------------------------------------------------------------
dt = pi/n;
if strcmp(grd, 'pole') || strcmp(grd, 'mesh')
   theRAD = (0:dt:pi)';
   lamRAD = (0:dt:2*pi-dt);
elseif strcmp(grd, 'block') || strcmp(grd, 'cell')
   theRAD = (dt/2:dt:pi)';
   lamRAD = (dt/2:dt:2*pi);
elseif strcmp(grd, 'neumann') || strcmp(grd, 'gauss')
   [gx, ~] = grule(n+1); 
   theRAD = flipud(acos(standing(gx)));
   lamRAD = (0:dt:2*pi-dt);
else
   error('What type of grid do you want?')
end
nlat = length(theRAD);
nlon = length(lamRAD);

% -------------------------------------------------------------------------
% Preprocessing on the coefficients: 
%    - subtract reference field (if jflag is set)
%    - specific transfer
%    - upward continuation
% -------------------------------------------------------------------------
if jflag 
    field = field - cs2sc(normalklm(lmax)); 
    warning('A reference field (WGS84) is removed from your coefficients')
end

l = standing(0:lmax);
transf = eigengrav(l, quant, h);
field  = field .* (transf * ones(1, 2*lmax+1));
% -------------------------------------------------------------------------
% Size declarations and start the waitbar:
% Note that the definition of dlam causes straight zero-padding in case N > L.
% When N < L, there will be zero-padding up to the smallest integer multiple
% of N larger than L. After the Fourier transformation (see below), the
% proper samples have to be picked out, with stepsize dlam.
% -------------------------------------------------------------------------
dlam   = ceil(lmax / n);				% longitude step size
abcols = dlam * n + 1;				% # columns required in A and B
a      = zeros(nlat, abcols);
b      = zeros(nlat, abcols);
hwb    = twaitbar('init', [], 'GSHS Percentage of orders m ready ');

% -------------------------------------------------------------------------
% Treat m = 0 separately
% -------------------------------------------------------------------------
m         = 0;
c         = field(m+1:lmax+1, lmax+1+m);
p         = plm(m:lmax, m, theRAD);
a(:, m+1) = p * c;
b(:, m+1) = zeros(nlat,1);
hwb       = twaitbar((m+1) / (lmax+1), hwb); 		% Update the waitbar

% -------------------------------------------------------------------------
% Do loop m = 1 to lmax
% -------------------------------------------------------------------------
for m = 1:lmax
   c         = field(m+1:lmax+1, lmax+1+m);
   s         = field(m+1:lmax+1, lmax+1-m);
   p         = plm(m:lmax, m, theRAD);
   a(:, m+1) = p * c;
   b(:, m+1) = p * s;
   hwb       = twaitbar((m+1) / (lmax+1), hwb);			% Update the waitbar
end

clear field
twaitbar('close', hwb);

% -------------------------------------------------------------------------
% The second synthesis step consists of an inverse Fourier transformation
% over the rows of a and b. 
% In case of 'block', the spectrum has to be shifted first.
% When no zero-padding has been applied, the last b-coefficients must be set to
% zero. Otherwise the number of longitude samples will be 2N+1 instead of 2N.
% For N=L this corresponds to setting SLL=0!
% -------------------------------------------------------------------------
if strcmp(grd, 'block') || strcmp(grd, 'cell') 
   m      = 0:abcols-1;
   cshift = ones(nlat, 1) * cos(m*pi/2/n);	% cshift/sshift describe the 
   sshift = ones(nlat, 1) * sin(m*pi/2/n);	% half-blocksize lambda shift.
   atemp  =  cshift.*a + sshift.*b;
   b      = -sshift.*a + cshift.*b;
   a      = atemp;
end

if rem(n,lmax) == 0				% Case without zero-padding.
   b(:,abcols) = zeros(nlat,1);	
end

f = ispec(a',b')';
if dlam > 1, f = f(:,1:dlam:dlam*nlon); end
