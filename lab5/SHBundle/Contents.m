% Spherical Harmonic Computation and Graphics tools, v.2014
% Requires uberall toolbox
%
%-----------------------------------------------------------------------
% SH Synthesis & Analysis:
%-----------------------------------------------------------------------
%   eigengrav     - isotropic spectral transfer (eigenvalues)
%   gradpshs      - Global Spherical Harmonic Synthesis - gradient
%   gsha          - Global Spherical Harmonic Analysis
%   gshs_         - Global Spherical Harmonic Synthesis
%   gshs_grid     - Global Spherical Harmonic Synthesis - on any grid
%   gshs_ptw      - Global Spherical Harmonic Synthesis - pointwise
%   lovenr        - Love-numbers for the deformation of the elastic Earth
%   pshs          - pointwise Spherical Harmonic Synthesis
%   regionalshs   - Regional spherical harmonic synthesis on regular grids
%   shspkamph     - Amplitude and phase of spherical harmonic spectrum
%   sph_gradshs   - creates a SINGLE POINT HANDLE for calculating the
%                   gradient of a potential
%   tenspshs      - pointwise Spherical Harmonic Synthesis of the Marussi
%                   tensor 
%   upwcon        - upward continuation in the spectral domain
%   updwcont      - pointwise upward continuation in the spatial domain
% DEPRECATED:
%   gshsptw       - will be removed in the future! (replaced by gshs_ptw)
%   gshs          - will be removed in the future! (replaced by gshs_)
%   gshsag        - will be removed in the future! (replaced by gshs_grid)
%   rshs          - will be removed in the future! (replaced by regionalshs)
%-----------------------------------------------------------------------
% SH Synthesis of two-point functions on the sphere:
%-----------------------------------------------------------------------
%   covord        - converts covariance matrix into a four variable cell
%                   structure
%   gshscov       - Synthesizes SH spectral covariance matrices to spatial
%                   covariances
%   gshs2ptfun    - Same as GSHSCOV but used for computing the covariance
%                   function/bi-polar field for a particular point on the
%                   sphere iand for any type of grid (also individual points).
% DEPRECATED:
%   gshscovfn     - will be removed in the future! (replaced by gshs2ptfun)
%   gshscovfnag   - will be removed in the future! (replaced by gshs2ptfun)
%-----------------------------------------------------------------------
% Legendre polynomials & SH:
%-----------------------------------------------------------------------
%   diffLegpol     - unnormalized Legendre polynomials and their 1., 2. and
%                   3. derivatives  
%   iplm          - integrated Legendre functions
%   legendreP     - all Legendre functions up to degree n and the 1. 
%                   and 2. derivatives 
%   legendreP_Xnum - the same as legendreP, recommended for degree/order > 1500
%   Legendre_mex  - fully normalized Legendre functions of first kind up to
%                   degree n (order of coefficients different to 
%                   legendreP); uses X-numbers for a stable computation
%                   from degree/order >= 1500.
%   Legendre0     - normalized Legendre functions at the equator, with
%                   exact zeros of odd functions and in the triangle format
%                   (for construction the inclination functions via products)
%   legpol        - unnormalized Legendre functions
%   nablaPot      - radial derivatives for the disturbing potential along
%                   the orbit 
%   neumann       - weights and nodes for Neumann's quadrature methods
%   plm           - Legendre functions (normalized) and derivatives, consider
%                   using Legendre_mex for speed!
%   ylm           - surface spherical harmonics
% DEPRECATED:
%   plmspeed      - will be removed in the future! (replaced by Legendre_mex)
%                   Legendre functions (normalized) only, no derivatives
%
%-----------------------------------------------------------------------
% Visualization:
%-----------------------------------------------------------------------
%   ylmplot       - plot surface spherical harmonic
%   shplot        - plot SH spectrum
%   shprepare     - prepare SH spectrum for plotting purposes
%
%-----------------------------------------------------------------------
% SH coefficients storage format conversion:
%-----------------------------------------------------------------------
%   cs2sc         - CS-format to SC-format conversion (consult
%                   documentation for format explanations) 
%   sc2cs         - SC-format to CS-format conversion
%   cs2vec        - CS-format to column-vector-format
%   vec2cs        - Column-vector-format to CS-format
%   cssc2clm      - CS/SC-format to indexed-column-vector format
%   clm2sc        - Indexed column-vector-format to SC-format, with 
%                   maximum degree and order (replaces gcoef2sc and
%                   icgem2sc)
%   clm2klm       - Convert order-wise look-up-table format to degree-wise
%   klm2clm       - Converts back from degree-wise look-up-table format
%                   to order-wise
%   legendreIndex - indices to select spherical harmonic coefficients of
%                   certain degree/order 
%   parse_icgem   - Reads SH coefficients from ICGEM-format ASCII files
%                   and returns them in indexed column-vector-format
%   sortLegendre  - conversion between degree-order format and 
%                   order-degree format.
%   checkshformat - Checks the format of the input SH coefficients
% DEPRECATED:
%   gcoef2sc      - will be removed in the future! (replaced by clm2sc)
%                   Indexed-column-vector-format to SC-format 
%
%-----------------------------------------------------------------------
% Statistics:
%-----------------------------------------------------------------------
%   degreeinfo    - SH information per degree (signal or error degree RMS;
%                   commission error) 
%   degcorr       - SH degree correlation  
%   kaula         - Kaula rule
%   ordrms        - SH information per order (order RMS)
%   globalmean    - Computes the global mean of the field
%   globalpower   - Computer the power of a given field
%
%-----------------------------------------------------------------------
% Complex SH co-efficients to real & vice-versa:
%-----------------------------------------------------------------------
%   cpx2realsh    - Complex to real conversion for coefficients
%   cpx2realmat   - Complex to real conversion for a particular SH-degree
%   cpx2realcov   - Complex to real conversion for covariance matrices of
%                   SH spectrum 
%   real2cpxsh    - Real to complex conversion for coefficients
%   real2cpxmat   - Real to complex conversion for a particular SH-degree
%   real2cpxcov   - Real to complex conversion for covariance matrices of
%                   SH spectrum 
%   LeNorm        - factor between geodetic and complex normalization of
%                   Legendre functions 
%
%-----------------------------------------------------------------------
% Normal field:
%-----------------------------------------------------------------------
%   normalklm     - normal field coefficients
%
%-----------------------------------------------------------------------
% Testing:
%-----------------------------------------------------------------------
%   iplmquad      - IPLM by numerical quadrature
%
%-----------------------------------------------------------------------
% Helpers  
%-----------------------------------------------------------------------
%   mex_compile   - contains the command for compiling the mex-functions
%
%-----------------------------------------------------------------------
% Data:
%-----------------------------------------------------------------------
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% authors: 
%     Nico Sneeuw (NS)
%     Matthias Weigelt (MW)
%     Markus Antoni (MA)
%     Balaji Devaraju (BD)
%     Matthias Roth (MR)
%
% file compilation: 
%            2015-09-02, MR, add mex_compile
%            2015-07-28, BD, declare some functions deprecated, add replacements
%            2014-10-08, MR, declare some functions deprecated
%   v.2014:  2014-01-15, MR, check all files: revise help texts, beautify code
%   v.2013b: 2013-02-13, MR, consolidation of function names, small bug fixes
%     v.5:   2013-01-18, MA/BD, integration of new tools/update of contents-file
%     v.4:   2012-06-12, MW, added or revision: cs2sc, cs2vec, degcorr, gaussian,
%                            gradpshs, gshsag, gshsptw, nablaPot, normg, pshs, 
%                            sc2cs, tenspshs, updwcont, vec2cs
%     v.3:   2008-08-12, NS, included pointwise shs and numerous other files
%     v.2:   2002-05-22, NS, included ylm, shplot, ylmplot and ancillary files
%     v.1:   2000-10-05, NS, initial version






   
                                       
