function [varargout] = gcoef2sc(xyz)

% WARNING! GCOEF2SC IS DEPRECATED!
% Please replace all function calls of GCOEF2SC in your code by calls to
% CLM2SC which offers a wider range of functionality:
%   old: sc = gcoef2sc(clm);         --> new: sc = clm2sc(clm);
%   old: [sc, s, c] = gcoef2sc(clm); --> new: [sc, s, c] = clm2sc(clm, 'gcoef2sc', true);
%
% GCOEF2SC converts a column-based geopotential co-efficients file into a
% SC-format matrix.
% 
% IN:
%    xyz ......... file format : [l m Clm Slm]
%
% OUT: 
%    varargout ... SC-format matrix of the geopotential co-efficients
%
% USES:
%    xyz2mat

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Nico SNEEUW (NS), IAGP, TU Munich
%    Mohammad SHARIFI (MS), GI, Uni Stuttgart 
%    Balaji DEVARAJU (BD), GI, Uni Stuttgart
%    Matthias ROTH (MR), GI, Uni Stuttgart
%    <bundle@gis.uni-stuttgart.de>
% -------------------------------------------------------------------------
% revision history:
%    2014-09-25: MR, set function to deprecated, will be replaced by CLM2SC
%    2013-02-13: MS, converted the M-file into a function
%    2007-03-09: BD, modification 
%    1996-??-??: NS, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

warning('MATLAB:deprecated', 'DEPRECATED! This function will be removed in a future version of SHbundle.\nUse CLM2SC instead. \n--> Please check the help to this function and update your code!');

if ischar(xyz)
    xyz = load(xyz);
end

clm = xyz2mat(xyz,1e-40)';
slm = xyz2mat(xyz,1e-40,4)';

varargout{2} = clm;
varargout{3} = slm;
slm(:,1) = [];

varargout{1} = [fliplr(slm) clm];
