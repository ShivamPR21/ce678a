function rgb = colbrew(typ, colmapsize, maxmidmin)

% COLBREW creates a colormap, different styles are possible. 
% Some color schemes are chosen--according to "www.colorbrewer2.org"--to be 
% colorblind safe (marked below as CBS)
%
%   COLBREW
%   COLBREW(typ)
%   COLBREW(typ, colmapsize)
%   COLBREW(typ, colmapsize, maxmidmin)
%
% OPTIONAL: 
%     typ ......... 1: blue - light yellow - red (CBS, default)
%                   2: blue - white - red (CBS)
%                   3: blue - white (CBS)
%                   4: cyan - white - brown (CBS)
%                   5: violet - white - orange (CBS)
%                   6: blue - green - yellow - orange - red
%                   7: blue - cyan - yellow - pink - blue (cyclic)
%                   8: corporate design of University of Stuttgart
%     colmapsize .. size of colormap (default: retrieved from set colormap)
%     maxmidmin ... maximal, middle and minimal value as array [max mid min]
%                   only needed for colorbars with a middle value not 
%                   centered on the scale. This feature will only work
%                   if max and min values are corresponding to your data. 
%                   (default: [1 0 -1])
% OUT:
%    rgb .......... colormap                                [colmapsize, 3]
% SEE ALSO:
%    REDBLUE, GRAY

% ----------------------------------------------------------------------------
% authors:
%    Matthias ROTH (MR), GI, Uni Stuttgart
% ----------------------------------------------------------------------------
% revision history:
%   2018-08-30: PH, add colormap with corporate design
%   2015-09-10: MR, small improvement in color generating
%   2015-09-01: MR, add a cyclic color scheme
%   2014-09-10: MR, add missing description in helptext
%   2013-04-10: MR, rename function to COLBREW, add more color schemes, 
%                   correct a small bug, brush up comments
%   2013-04-09: MR, brush up comments
%   2009-05-03: MR, initial version
% ----------------------------------------------------------------------------

%% define colormaps
stations{1} = [ 1  49  54 149;  % blue
                2  69 117 180;
                3 116 173 209;
                4 171 217 233;
                5 224 243 248;
                6 255 255 191;  % light yellow
                7 254 224 144;
                8 253 174  97;
                9 244 109  67;
               10 215  48  39;
               11 165   0  38]; % red

stations{2} = [ 1   5  48  97;  % blue
                2  33 102 172;
                3  67 147 195;
                4 146 197 222;
                5 209 229 240;
                6 247 247 247;  % white
                7 253 219 199;
                8 244 165 130;
                9 214  96  77;
               10 178  24  43;
               11 103   0  31]; % red
           
stations{3} = [ 1  54 144 192;  % blue  
                2 116 169 207;
                3 166 189 219;
                4 208 209 230;
                5 236 231 242;
                6 255 247 251]; % white

stations{4} = [ 1   1 102  94;  % cyan
                2  53 151 143;
                3 128 205 193;
                4 199 234 229;
                5 245 245 245;  % white
                6 246 232 195;
                7 223 194 125;
                8 191 129  45;
	            9 140  81  10]; % brown
           

stations{5} = [ 1  84  39 136;  % violet
                2 128 115 172;
                3 178 171 210;       
                4 216 218 235;
                5 247 247 247;  % white
                6 254 224 182;
                7 253 184  99;       
                8 224 130  20;
                9 179  88   6]; % orange         

stations{6} = [ 1  50 136 189;  % blue 
                2 102 194 165;
                3 171 221 164;  % green
                4 230 245 152;
                5 255 255 191;  % yellow
                6 254 224 139;
                7 253 174  97;  % orange
                8 244 109  67;
                9 213  62  79]; % red    

% cyclic colour map
stations{7} = [ 1   0   0 255;  % blue 
                2   0 255 255;  % cyan
                3 255 255   0;  % yellow
                4 255   0 255;  % pink
                5   0   0 255]; % blue  
% corporate design color map            
stations{8} = [ 1  62  68  76;  % anthrazit
                2 123 127 132;
                3 184 186 188;
                4 215 216 217;
                5 245 245 245;  % (almost) white
                6 184 232 248;
                7 123 218 250;
                8   0 190 255;  % light-blue
                9   0  81 158]; % mid-blue
                
                
%% check input parameters
if ~exist('typ', 'var')
    typ = 1;
end

if (typ < 1) || (typ > length(stations))
    warning(sprintf('"typ" must be a full number within [1, %d].\nNow switching to default values: typ = 1.', length(stations)));
    typ = 1;
end

if ~exist('colmapsize', 'var')
    colmapsize = size(get(gcf, 'colormap'), 1); 
end

if ~exist('maxmidmin', 'var')
    maxmidmin = [1 0 -1];
end

if max(size(maxmidmin) ~= [1 3])
    warning('"maxmidmin" must be a vector of the form [max mid min].\n Now switching to default values: [1 0 -1].');
    maxmidmin = [1 0 -1];
end

%% calculate colormap
m   = size(stations{typ}, 1);
x   = stations{typ}(:, 1);
m0  = mean(x);
mmm = (maxmidmin - maxmidmin(2)) / max(abs(maxmidmin([1 3]) - maxmidmin(2)));
mx  = mmm * (m0 - 1) + m0;

xi  = linspace(min(mx), max(mx), colmapsize);
rgb = interp1(x, stations{typ}(:, 2:4) / 255, xi, 'pchip'); % shape-preserving piecewise cubic interpolation
