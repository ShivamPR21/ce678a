function AXES = extendFrame(x,y,z)
% AXES = extendFrame(x,y,z)
%
% EXTENDFRAME increases the axis by 10 % in each direction for a 'better' 
% visualization. The routine can also be used for zooming by using only the
% data of interest for determination of axis. 
% 
% IN:
% x:    coordinate in x direction                                          [NxN]
% y:    coordinate in y direction                                          [NxN]
% z:    coordinate in z direction                                          [NxN]
%
% OUT:
% AXES: vector of 4 respectivly 6 elements, so that axis(AXES) shows all   [6x1]
%       elements in the graphic
% 
%
% EXAMPLE:
%        clearvars  -except CONSTANT_EARTH_MODELL
%        x = rand(10,10);
%        y = 10+randn(10,10);
%        z = 1000+randn(10,10);
%        subplot(221)
%        AXES = extendFrame(x,y,z);
%        plot3(x,y,z,'k*');axis(AXES);
%
%        subplot(222)
%        plot(x,y,'o'); axis(extendFrame([x(:),y(:)]));

%**************************************************************************
% author:               Markus ANTONI
% created at:           06.03.2013
% last modification     19.08.2013 
% project:              visualisation tools
%**************************************************************************
if nargin<3
    z = [];
end
if nargin<2
    % in case of a single input the coordinates are extracted from the first
    % argument
    [row,col] = size(x);
    if col == 3
        z = x(:,3);
        y = x(:,2);
        x = x(:,1);
    elseif col == 2
        y = x(:,2);
        x = x(:,1);
    else
        if col == 3
            z = x(3,:);
            y = x(2,:);
            x = x(1,:);
        elseif col == 2
            y = x(2,:);
            x = x(1,:);
        else
            error('for one input into ''extendFrame'', x must be a [Nx2] or [Nx3] matrix')
        end
    end
end

x = x(:);
y = y(:);

% removing Inf, NaN ...
index = isfinite(x);
x = x(index);

index = isfinite(y);
y = y(index);

% minimal and maximal values
minx = min(x);
maxx = max(x);
miny = min(y);
maxy = max(y);


AXES = [minx,maxx,miny,maxy];

if isempty(z)==0
    % considering the 3 dimension if necessary
    z = z(:);
    index = isfinite(z);
    z = z(index);
    minz = min(z);
    maxz = max(z);
   
    AXES  = [AXES, minz, maxz];
end

% increasing the 'frame', so that all corners are visible
for ii = 1:2:length(AXES);
    dx = abs(AXES(ii)-AXES(ii+1))/10;
    if dx <eps
        % avoid equal limits in x,y,z (data within a plane)
        dx  = AXES(ii)/1e6;
    end
    % increase limits by 10 percent
    AXES(ii) = AXES(ii)-dx;
    AXES(ii+1) = AXES(ii+1)+dx;
    
    
end
    

end