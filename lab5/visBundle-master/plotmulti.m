function HANDLE = plotmulti(x,data,ylimits,yticks,xticks,titleStr,color)
% HANDLE = plotmulti(x,data,ylimits,yticks,xticks,titleStr,color)
%
% PLOTMULTI visualizes several one-dimensional data sets with the same time
% axes in one figure and with repeated y-axes. A possible application is the 
% comparison of several instruments (e.g. gravimeters) in one location. 
%
% For readablitity, only 12 data sets are allowed!
%
% IN:
% x:        argument/time                                                  [Nx1]
% data:     data set/observations                                          [NxM]
% ylimits:  maximum/minimum of y-axis                                      [2x1]
% yticks:   labels of y-axis                                               [nx1]
% xticks:   labels of x-axis                                               [mx1]
% titleStr: title of the figure                                            ['string']
% color:    linestyle                                                      [cell]
%
% OUT:
% HANDLE:   handle of the graphics for further improvement
%
% See also: GRAYPATCH
%
% sub-routines:
%    *   --
%
% example
% plotmulti(1:100,randn(100,4),[-3,3],[-2:2],[0:10:100])
% plotmulti(1:100,randn(100,4),[-3,3],[-2:2],[0:10:100],'data sets')
% plotmulti(1:100,randn(100,4),[-3,3],[-2:2],[0:10:100],'data sets in black',{'k'})

%**************************************************************************
% author:               Markus ANTONI
% created at:           24.01.2018
% last modification     29.01.2018
%**************************************************************************

%% default settings
if nargin < 7
    color = {'r','b','k','g','r:','b:','k:','g:'};
end
if iscell(color) == 0
    color = [];
    warning('''color'' should be given as cell-array')
end
if isempty(color) == 1
    color = {'r','b','k','g','r:','b:','k:','g:'};
end
if nargin < 6
    titleStr = [];
end
if ischar(titleStr) == 0
    titleStr = [];
    warning('''titleStr'' should be a string')
end
if isempty(titleStr) == 1
    titleStr = 'Multiple data sets';
end


%% checking the input of 'x' and 'data'
if isvector(x) == false
    error('argument ''x'' must be a vector')
end
if ismatrix(data) == false
    error('''data'' must be a matrix')
end


x = x(:);
x_anz = numel(x);
[row,col] = size(data);
if ismember(x_anz, [row,col]) == 0
    error('data set must have the same length as ''x''')
end
if row ~= x_anz
    data = data';
    col = row;
end
if col > 12
    error('These figures are too small. Please split the data set')
end
%% checking limits and labels
switch numel(ylimits)
case 2
    % everything ok
case 1
    ylimits = [-ylimits,ylimits];
otherwise
    error('interval ''ylimits'' must be a vector [2x1]')
end
xlimits = [min(x),max(x)];
if isvector(xticks) == false
    error('argument ''xticks'' must be a vector')
end
if isvector(yticks) == false
    error('argument ''yticks'' must be a vector')
end
xticks  = unique(xticks);
yticks  = unique(yticks);
xtickrm = xticks(2:end-1);

%% plot several overlapping subplots 
height = 0.9/(col+1);
iter = 1;
iterMax = length(color);
for ii = col:-1:1
    subplot('Position',[0.1,ii*height,0.8,height])
    HANDLE(ii) = plot(x,data(:,ii),color{iter});
    ylim(ylimits)
    xlim(xlimits)
    set(gca,'YTick',yticks)
    if ii > 1
        set(gca,'XTick',xtickrm)
    else
        set(gca,'XTick',xticks)
    end
    if ii == col
        try
            title(titleStr) 
        catch
        end
    end
    legend(['data:' num2str(ii)])
    if rem(ii,2) == 1
        set(gca,'Color',[0.94 0.94 0.94])
    end
    iter = iter+1;
    if iter > iterMax
        iter = 1;
    end
end
