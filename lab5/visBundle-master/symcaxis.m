function cax = symcaxis(signal,minmax)
% cax = symcaxis(signal)
% 
% SYMCAXIS centers the colorbar if
%   * the values are positive and negative
%   * and of similar order of magnitude (tested by log10),
% otherwise the colorbar is cut off at the minimum/maximum. The cut-off at 
% minimum/maximum can be forced by using 'minmax' = true as input argument.
%
% IN:
% signal: data to be visualized                                            [NxN]
% minmax: cut-off without trying symmetry (true) or with symmetry    [bool][1x1]
%         (false: <default>)    
% 
% OUT:
% cax: minimum and maximum of the colorbar                                 [2x1]
%
% sub-routines:
%    *   --
%
% EXAMPLE:
%        clearvars  -except CONSTANT_EARTH_MODELL
%        
%        F = magic(100); ii = randn(100)<0 ; F(ii) = -F(ii); 
%        figure; imagesc(F);  caxis(symcaxis(F(:)));colorbar
% 
%        P = peaks
%        figure; surf(P);  caxis(symcaxis(P));colorbar
%        figure; surf(P);  caxis(symcaxis(P,true));colorbar

%**************************************************************************
% author:               Markus ANTONI
% created at:           14.03.2013 
% last modification     22.03.2013 
%                       08.09.2014
% project:              visualisation tools
%**************************************************************************
if nargin>1
    if isscalar(minmax)== 0 || islogical(minmax) == 0
        minmax = false;
    end
else
    minmax = false;
end
    
signal = signal(:);
ii0 = isfinite(signal);
signal = signal(ii0);
MIN =  min(signal);
MAX =  max(signal);
cax = [MIN,MAX];
if minmax == 0
    if MIN*MAX<0
        % positive and negative values:
        if abs(fix(log10(MIN)) - fix(log10(MAX)))<2;
            % |log10(x)-log10(y)| < 2 ==> |x|/|y| <100 ==> similar magnitude 
            disp('introducing a symmetric colorbar')
            V = max([abs(MIN),MAX]);
            cax = [-V,V];
        end
    end; 
end;

% special (and rarely) case: constant values in an image
% The implementation above produces an error, which is avoided by adding a
% very small number 
if abs(cax(2)-cax(1))<eps
    cax(2) = cax(2)+10*eps;
    cax(1) = cax(1)-10*eps;
end
