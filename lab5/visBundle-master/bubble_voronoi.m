function [V, C] = bubble_voronoi(x, y, maxradius, anz)

% BUBBLE_VORONOI calculates a kind of Voronoi diagram for points 
% given by the coordinates (x,y), but cuts each Voronoi cell at a 
% circular polygon around each point which is defined by maxradius 
% and (optionally) the amount of polygon points anz.
%
% IN:
%   x, y ........ coordinates of points
%   maxradius ... maximum radius of a cell 
%   anz ......... maximum number of polygon points (default: 20)
% OUT:
%   V ........... vertices (points of the cell borders)
%   C ........... cells (indices of the points of each cell in V)
% EXAMPLE:
%   x = randn(10, 1);
%   y = randn(10, 1);
%   c = randn(10, 1);
%   [V, C] = bubble_voronoi(x, y, 1.5);
%   figure; hold on;
%   for i = 1:length(x)
%       patch(V(C{i}, 1), V(C{i}, 2), c(i));
%   end
%   plot(x, y, 'k.');
%   cmax = max(c); cmin = min(c);
%   shading flat; caxis([cmin cmax]); 
%   colormap(colbrew(1, 256, [cmax 0 cmin]));
%   axis equal;

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Matthias ROTH (MR), GI, Uni Stuttgart
% -------------------------------------------------------------------------
% revision history:
%    2015-02-20: MR, revision/translation of help text, add example
%    2010-02-23: MR, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

ps = size(x, 1);
if length(maxradius) ~= length(x)
    r = ones(ps, 1) * maxradius;
else
    r = maxradius;
end

if ~exist('anz', 'var')
    anz = 20; % set polygon point number default
end

t       = linspace(0, 2*pi*(anz-1)/anz, anz);
idx_raw = linspace(1, ps, ps)';

V = [];
C = {};

for j = 1:ps
%    fprintf('%d\n', j);
    if size(r, 2) == 1
        Kx = x(j) + r(j) * cos(t);
        Ky = y(j) + r(j) * sin(t);
    else
        Kx = x(j) + r(j, 1) * cos(t);
        Ky = y(j) + r(j, 2) * sin(t);
    end
    P  = [Kx(:), Ky(:)];
    
    xy = sortrows([x - x(j), y - y(j), idx_raw], 1); % remove all points which are further away
    xy = xy(abs(xy(:, 1)) < 2 * r(j), :);
    xy = sortrows(xy, 2);
    xy = xy(abs(xy(:, 2)) < 2  * r(j), :);
    
    for i = xy(:, 3)' % calculate perpendicular bisectors to nearby points
        if i == j, continue, end
        
        delta_x = x(j) - x(i);
        delta_y = y(j) - y(i);
        d = sqrt(delta_x^2 + delta_y^2);
        
        if (d > r(j) + r(i)) || ...      % no solutions, the circles are separate. 
           (d < abs(r(j) - r(i))) || ... % no solutions because one circle is contained within the other.
           (d == 0)                      % r(j) = r(i), the circles are coincident and there are an infinite number of solutions.
            continue;
        end
    
        a = (r(i)^2 - r(j)^2 + d^2) / (2 * d);
        h = sqrt(r(i)^2 - a^2);

        m = [x(i); y(i)] + a / d * [delta_x; delta_y];
        sx = m(1) + h * [2; -2] * delta_y / d;
        sy = m(2) + h * [-2; 2] * delta_x / d;

        P = cutpoly(P, [sx, sy], [x(j), y(j)]);
    end

    c = (1:size(P, 1)) + size(V, 1);
    C{end+1, 1} = c;
    V = [V; P];
end


%% necessary functions for bubble_voronoi
function a = ccw(p0, p1, p2) % Counterclockwise
dx1 = p1(1) - p0(1); dy1 = p1(2) - p0(2);
dx2 = p2(1) - p0(1); dy2 = p2(2) - p0(2);
if dx1 * dy2 > dy1 * dx2 
    a = 1;
elseif dx1 * dy2 < dy1 * dx2
    a = -1;
else
    if (dx1 * dx2 < 0) || (dy1 * dy2 < 0)
        a = -1;
    elseif (dx1^2 + dy1^2) >= (dx2^2 + dy2^2)
        a = 0;
    else 
        a = 1;
    end
end

function newpoly = cutpoly(poly, line, point)
n = length(poly);
ccwpoint = ccw(point, line(1, :), line(2, :));
for i = 1:n
    cutnotoff(i) = ccw(poly(i, :), line(1, :), line(2, :)) == ccwpoint;
end

newpoly = [];
for i = 1:n
    j = mod(i, n) + 1;
    if ~(cutnotoff(i) || cutnotoff(j)), continue, end
    if cutnotoff(i) && cutnotoff(j)
        newpoly(end + 1, :) = poly(i, :);
    else % neuer Punkt
        if cutnotoff(i) && ~cutnotoff(j) % plus ein alter
            newpoly(end + 1, :) = poly(i, :);
        end
        xx1 = poly(i, 1);   yy1 = poly(i, 2); 
        xx2 = poly(j, 1);   yy2 = poly(j, 2); 
        xx3 = line(1, 1);   yy3 = line(1, 2);
        xx4 = line(2, 1);   yy4 = line(2, 2);
        
        d = (xx1-xx2) * (yy3-yy4) - (yy1-yy2) * (xx3-xx4);
        xp = ((xx1*yy2 - yy1*xx2) * (xx3 - xx4) - (xx1 - xx2) * (xx3*yy4 - yy3*xx4)) / d;
        yp = ((xx1*yy2 - yy1*xx2) * (yy3 - yy4) - (yy1 - yy2) * (xx3*yy4 - yy3*xx4)) / d;

        newpoly(end + 1, :) = [xp, yp];
    end
end






