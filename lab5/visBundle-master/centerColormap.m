function CM = centerColormap(MIN,MAX,type)
% CM = centerColormap(MIN,MAX,type)
%
% CENTERCOLORMAP tries to adapt "two-color-colormaps" (e.g. redblue-style) 
% in such a way, that positive and negative values are visible separated. 
% The zeros and small values are referring to the middle of the original 
% colormap.  
%
% In the redblue*-colormaps the positive part should be red, the
% negative values are blue and the very small values remain in white.
%
% The algorithm requires an odd dimension of the colormap
% (at the moment: 201 elements). 
% 
% IN:
% MIN:  minimal value after cutting the colormap                           [1x1]
% MAX:  maximal value after cutting the colormap                           [1x1]
% type: existing colormap, either as [Nx3] matrix or by its name
%
% OUT:
% CM:   asymmetric centered colormap                                       [Nx3]
%
% See also: --
%
% sub-routines:
%    *   --
%
% EXAMPLE:
%        F = peaks(100);
%        figure; surf(F);centerColormap(-2,8,'redblue1'); 
%        figure; surf(F);centerColormap(-12,12,'redblue1');  
%        figure; surf(F);centerColormap(-12,20,'redblue5');        
%        figure; surf(F);centerColormap(-50,2,'redblue7');     
%        figure; surf(F);centerColormap(-10,2,'gray');

%**************************************************************************
% author:               Markus ANTONI
% created at:           08.03.2013
% last modification     08.09.2014
% project:              visualisation tools
%**************************************************************************

%% check of input arguments
if nargin<3
   type  = redblue1;
end

if isscalar(MAX) == 0 || isscalar(MIN) == 0
    error('''MAX'' and ''MIN'' of colorbar must be scalar values')
end

if MAX<MIN
    varhelp = MIN ;
    MIN = MAX;
    MAX = varhelp;
end
if MIN*MAX>0
    % improvement only for different signs of MAX and MIN
    return;
end
MINMAX = [MIN,MAX];

%% dimensions fixed for the moment to 201 (latter improvement?)
SIZE = 201;
minus = (SIZE-1)/2;

if isnumeric(type) == 1
    
    [row,col] = size(type);
    if col ~=3
        error('colormaps must be given in [Nx3]-format')
    end
    if row ~=201
        error('At the moment, a [201x3]-matrix is required')
    end
    CM = type;
else
    try
        CM = colormap([type '(' num2str(SIZE) ')']);
    catch
        error('colormap was not found')
    end
end


%% idea: assume the "two-color-colormaps" to be symmetric and cut the 
% positive/negative axis in the brighter "area"
MIN = abs(MIN) ;
colorbar off
if MIN<MAX
    varhelp = fix((1-MIN/MAX)*minus)+1;   
    CM(minus-varhelp:minus,:) = [];
    
else
    varhelp = fix((1-MAX/MIN)*minus);
    plus = minus+2;
    CM(plus:plus+varhelp,:) = [];
end


%% use the result in the plot:
colormap(CM);
colorbar
caxis(MINMAX)