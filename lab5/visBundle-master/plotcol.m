function handle = plotcol(x,y,z,c,linestyle)

% PLOTCOL generates a line plot that uses the color map.
% 
% PLOTCOL(X,Y,Z,C,LINESTYLE) plots a colored parametric
% line based on X, Y, Z, and C using the line style
% LINESTYLE.  The color scaling is determined by the
% values of C or by the current setting of CAXIS.  The
% scaled color values are used as indices into the current
% COLORMAP.
%
% Any combination of inputs can be used.  If C is not
% given, it is assigned to Z, Y, or X, depending on the
% input.  Below is a table which describes this:
%
%   GIVEN       VALUE OF C
%   X,Y,Z           Z
%    X,Y            Y
%     X             X
%
% SEE ALSO: mesh

% uses none
%
% Revs: 05/12/96, NS: 
% - Input arguments are transformed into column vectors. Column vector, 
%   row vector and matrices are allowed now. (not always sensible)
% - 3d vs. 2d handling changed.
% - V5-update: additional plot symbols (and marker symbol fill-in)
% 11/99, NS: separate between linestyle and marker after mesh command

if nargin == 0; error('Requires at least one input'); end
def_linestyle = '-';			% default line style
do3d = 0;					% default 2D view

% Determine which inputs were given:
if nargin == 1;                 		% 2D: X given.

  y = x(:)*[1 1];
  z = zeros(size(y));
  x = (1:length(x))'*[1 1];
  c = y;
  linestyle = def_linestyle;

elseif nargin == 2;  

  x = x(:)*[1 1];
  z = zeros(size(x));
  if isstr(y)                   		% 2D: X and LINESTYLE given.
    linestyle = y;
    y = x;
    x = (1:length(x))'*[1 1];
  else                          		% 2D: X and Y given.
    y = y(:)*[1 1];
    linestyle = def_linestyle;
  end
  c = y;

elseif nargin == 3;

  x = x(:)*[1 1];
  y = y(:)*[1 1];
  if ~isstr(z)                  		% 3D: X, Y, and Z given
    z = z(:)*[1 1];
    c = z;
    linestyle = def_linestyle;
    do3d = 1;
  else                          		% 2D: X, Y, and LINESTYLE given
    linestyle = z;
    z = zeros(size(x));
    c = y;
  end

elseif nargin == 4 

  x = x(:)*[1 1];
  y = y(:)*[1 1];
  z = z(:)*[1 1];
  if isstr(c)                   		% 2D or 3D: X, Y, Z=C, and LINESTYLE
    linestyle = c;
    c = z;
    if isempty(findstr(linestyle,'.ox+*sdv^<>ph')), do3d = 1;  end
    						% so lines yield 3D, markers 2D
  else						% 3D: with X, Y, Z, C
    linestyle = def_linestyle;
    c = c(:)*[1 1];
    do3d = 1;
  end

elseif nargin == 5				% 3D: Everything given.

  x = x(:)*[1 1];
  y = y(:)*[1 1];
  z = z(:)*[1 1];
  c = c(:)*[1 1];
  do3d = 1;

end

h = mesh(x,y,z,c);
if isempty(findstr(linestyle,'.ox+*sdv^<>ph'))
   set(h,'LineStyle',linestyle)
else
   set(h,'LineStyle','none')
   set(h,'Marker',linestyle)
   set(h,'MarkerFaceColor','flat')	% fill-in markersymbols
end
if ~do3d, view(2), end
if nargout == 1
  handle = h;
end

