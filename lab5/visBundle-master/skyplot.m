function hsky = skyplot(azim, elev, varargin)

% SKYPLOT Polar coordinate plot using azimuth and elevation data.
%	SKYPLOT(AZIM,ELEV) makes a polar plot of the azimuth
%       AZIM [deg] versus the elevation ELEV [deg].
%       Negative elevation is allowed.
%       Azimuth is counted clock-wise from the North.
%	SKYPLOT(AZIM, ELEV, VARARGIN) uses the linestyle, specified
%       in the parameters VARARGIN (Default: '*').
%	    See PLOT for a description of legal linestyles.
%
% EXAMPLE:
%   azim = [304 301 294 285 272 256 240 228 219;
%           208 208 204 111  56  53  55  57 60]';
%   elev = [43 49 54 59 62 63 61 57 52;
%           68 75 82 88 81 74 67 61 54]';
%   skyplot(azim,elev, '-', 'linewidth', 3);
%
% SEE ALSO:
%    POLAR (this function is modelled after polar.m)

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Nico SNEEUW (SN), Munich
%    Matthias ROTH (MR), GI, Uni Stuttgart
% -------------------------------------------------------------------------
% revision history:
%    2015-09-08: MR, add functionality for additional plot-related 
%                    parameters like 'linewidth', brush-up help text
%    1996-01-05: NS, initial version
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------------- 

% Checks and stuff
if isstr(azim) | isstr(elev)
	error('Input arguments must be numeric.');
end
if any(size(azim) ~= size(elev))
	error('AZIM and ELEV must be the same size.');
end
if any(elev > 90  | elev < -90)
   error('ELEV within [-90;90] [deg].')
end
if nargin < 2
   error('Requires 2 or 3 input arguments.')
elseif nargin == 2 
   varargin = {'*'};
end

% get hold state
cax = newplot;
next = lower(get(cax,'NextPlot'));
hold_state = ishold;

% get x-axis text color so grid is in same color
tc = get(cax,'xcolor');

% only do grids if hold is off
if ~hold_state

% make a radial grid
   hold on;
% check radial limits and ticks
   zenmax = max(90-elev(:));
   zenmax = 15*ceil(zenmax/15);
   elmax  = 90;
% define a circle
   az     = 0:pi/50:2*pi;
   xunit  = sin(az);
   yunit  = cos(az);

% make solid circles each 30 deg, and annotate.
% The horizon (elev=0) is made thicker.
% Inbetween, and below horizon only dotted lines.
   for i=[30 60]
      plot(xunit*i,yunit*i,'-','color',tc,'linewidth',1);
   end
   i=90; plot(xunit*i,yunit*i,'-','color',tc,'linewidth',2);
   for i=[15:30:75 105:15:zenmax]
      plot(xunit*i,yunit*i,':','color',tc,'linewidth',1);
   end
   for i=30:30:zenmax
      text(0,i,[' ' num2str(90-i)],'verticalalignment','bottom');
   end

% plot spokes
   az = (1:6)*2*pi/12;                     % define half circle
   caz = cos(az); 
   saz = sin(az);
   ca = [-caz; caz];
   sa = [-saz; saz];
   plot(elmax*ca,elmax*sa,'-','color',tc,'linewidth',1);
   if zenmax > elmax
      plot(zenmax*ca,zenmax*sa,':','color',tc,'linewidth',1);
   end

% annotate spokes in degree
   rt = 1.1*elmax;
   for i = 1:length(az)
      loc1 = int2str(i*30);
      if i == length(az)
         loc2 = int2str(0);
      else
         loc2 = int2str(180+i*30);
      end
      text( rt*saz(i), rt*caz(i),loc1,'horizontalalignment','center');
      text(-rt*saz(i),-rt*caz(i),loc2,'horizontalalignment','center');
   end

% brush up axes
   view(0,90);
   axis(max(zenmax,elmax)*[-1 1 -1.1 1.1]);
   set(cax,'position',[.05 .05 .9 .9])
end


% transform data to Cartesian coordinates.
yy = (90-elev).*cosd(azim);
xx = (90-elev).*sind(azim);

% plot data on top of grid 
q = plot(xx, yy, varargin{:}); 
if nargout > 0, hsky = q;end

if ~hold_state
   axis('equal')
   axis('off')
end

% reset hold state
if ~hold_state, set(cax,'NextPlot',next); end

