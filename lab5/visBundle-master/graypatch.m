function h = graypatch(xStartEnd, Ylim, Xtick)
% graypatch(xStartEnd,Ylim,Xtick)
%
% GRAYPATCH hightlights 'periodic' events by marking every second period 
% with an alternating white/gray backgroud. Graypatch can also deal with 
% non-equal time spans (see example).
% 
% IN:
%   xStartEnd .. defines the time points of the events, 
%                [start, period_increment, end] for regular spacing   [1x3]
%                [T0, T1; T2, T3; ...] for irregular spacing          [Nx2]
%   Ylim ....... limits of y-axis                                     [1x2]
%   Xtick ...... numbers on the x-axis (e.g. periods T*n)             [1xN]
% OUT:
%   h .......... graphic handle
%
% EXAMPLE:
%   figure;
%   subplot(211);
%   t = -10:.01:30;
%   f1 = sin(t*pi/4);
%   plot(t, f1); hold on;
%   graypatch([-10,8,30], [-1.2,1.2], [-10:4:30]);
%   title('regular graypatch');
% 
%   subplot(212); 
%   f2 = sin(sign(t).*sqrt(abs(t))*pi).*exp(-t/10);
%   plot(t,f2); hold on;
%   xStartEnd = [-10,-4;0,4; 16,25];
%   graypatch(xStartEnd,[-3,3],[-10,-3,0,4, 15,25]);
%   title('irregular graypatch');

% -------------------------------------------------------------------------
% project: visbundle 
% -------------------------------------------------------------------------
% authors:
%    Markus ANTONI (MA), GI, Uni Stuttgart
%    Matthias ROTH (MR), GI, Uni Stuttgart
% -------------------------------------------------------------------------
% revision history:
%    2016-03-14: MR, brush-up help text; remove alpha bug: plotted gray
%                    bars on top of the data, hence changing their color;
%                    change code, that also fractions of periods are
%                    possible
%    2015-03-17: MA, some modifications
%    2015-02-19: MA, initial version, based on gray_patch of 
%                    Mohammad J. TOURIAN
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% ------------------------------------------------------------------------- 


%% create Start-End matrix from [start, period_increment, end]
if numel(xStartEnd) == 3
    blocks  = ceil((xStartEnd(3) - xStartEnd(1)) / (2 * xStartEnd(2))); 
    V(1, :) = xStartEnd(1):(2*xStartEnd(2)):xStartEnd(3);
    V(2, :) = V(1, :) + xStartEnd(2);
    V(V > xStartEnd(3)) = xStartEnd(3);
else % we expect non-periodic elements
    V      = xStartEnd';
    blocks = numel(V(1, :));
end

if blocks < 2
    warning('visbundle:notEnoughPeriods', 'not enough periods');
    return
end
% if rem(blocks, 2) == 1
%     V(end+1, :) = V(end, :);
%     blocks = blocks + 1;
% end

%% prepate patch coordinates 
X = V([1; 1; 2; 2], 1:blocks);
Y = repmat(Ylim([1, 2, 2, 1])', 1, blocks);
Z = ones(size(X));

%% gray blocks
h = patch(X, Y, Z, 'facecolor', [0.94 0.94 0.94], 'LineStyle', ':');
uistack(h, 'bottom');     % put gray blocks on lowest graphics layer
grid off; 

%% labeling, grid
Xlim = [min(V(:,1)),max(V(:,1))];
if nargin > 2
    if isvector(Xtick) == 1 && numel(Xtick) > 1
        set(gca, 'xtick', Xtick)
        Xlim = [min(Xtick), max(Xtick)];
        xlim(Xlim);
    else
        error('''Xtick'' must be given as a vector in increasing order')
    end    
end

%% add x- and y-axis 
if prod(Ylim) < 0
    plot(Xlim, [0,0], 'k', 'LineWidth', 2)
end
if prod(Xlim) < 0
    plot([0, 0], Ylim, 'k', 'LineWidth', 2)
end
ylim(Ylim);
