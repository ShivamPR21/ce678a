function plotcol4orbit(lamRAD,phiRAD,signal,relRadius,centred,unit)
% plotcol4orbit(lamRAD,phiRAD,signal,relRadius,centred,unit);
% 
% PLOTCOL4ORBIT visualizes one-dimensional data at arbitrary locations on 
% the sphere or along the orbit.
% 
%
% IN
% lamRAD:  longitude of the location                                  [rad][Nx1]
% phiRAD:  latitude of the location                                   [rad][Nx1]
% signal:  measurement value (height, potential, argument of latitude,..)[ ][Nx1]      
% relRadius: relative radius (r/R) with R = radius of the central sphere   [Nx1]   
%         (no visualization for relRadius <1)
% centred: view point towards the region of interest (centred = true)      [bool]
% unit:   unit of the functional (e.g. m, rad, m^2/s^2,...)                [string]
%
% OUT:
% H:     Handle of the figure
%
% See also: PLOTSPHERE2
%
% sub-routines:
%    *   plotcol
%    *   symcaxis
%    *   extendFrame
% 
% used data sets:
%    *   coast3.mat
%
% EXAMPLE:
%        clearvars  -except CONSTANT_EARTH_MODELL
%        figure;
%        subplot(221);plotcol4orbit(rand(1000,1),rand(1000,1),rand(1000,1))
%        subplot(222);plotcol4orbit(rand(1000,1),randn(1000,1),randn(1000,1),1.3,false,'mK')
 
%**************************************************************************
% author:               Markus ANTONI
% based on:             plotsphere2.m
% created at:           06.03.2013
% last modification     22.03.2013
% project:              visualization
%**************************************************************************
 
%% check of input arguments
if nargin<6
    unit = [];
end
if nargin<5
    centred = true;
end
if nargin<4
    relRadius = 1;
end
 
 
% standing vectors
lamRAD    = lamRAD(:);
phiRAD    = phiRAD(:);
relRadius = relRadius(:);
signal = signal(:);


%% dimension and units
try 
    [lamRAD,phiRAD,signal];
catch
    whos
    error('wrong:dimension','longitude, latitude and observations must have the same dimension!')
end
if max(abs(lamRAD))>2*pi 
    warning('check:unit','Is the longitude given in radian?')
end
if max(abs(phiRAD))>pi  
    warning('check:unit','Is the latitude given in radian?')
end
if min(relRadius)<1
    index = relRadius<1;
    signal(index) = NaN;
    warning('unusual:parameter','locations below the surface are ignored')
end
 
 
 
 
%% representing the signal by colors:
[x,y,z] = sph2cart(lamRAD,phiRAD,relRadius);
plotcol(x,y,z,signal,'.');hold on;
hCol = colorbar;
if ischar(unit)==1
    set(get(hCol,'title'),'String',['[' unit ']']);
end
caxis(symcaxis(signal));
 
%% adding the figure of the Earth
[X,Y,Z] = sphere(30);
surf(X,Y,Z,'FaceColor',[.9 .9 .9]); 
COAST = load('coast.mat');
RHO = pi/180;
[xC,yC,zC] = sph2cart(COAST.long*RHO,COAST.lat*RHO,1);
plot3(xC,yC,zC,'k')
axis equal;axis off;
 
%% changing the view point
if centred == 1
    view([mean(x),mean(y),mean(z)]);
    axis(extendFrame(x,y,z));
end

