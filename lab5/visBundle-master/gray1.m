function rgb = gray1(n);

% gray1(n) creates a colormap, ranging from dark black to white with a
% clear break at the center
%         
% Matthias Weigelt
% Stuttgart, 22/07/08


if nargin == 0, n = size(get(gcf,'colormap'),1); end

m    = ceil(n/2);
down = linspace(0,0.3,m);
up   = linspace(0.7,0.9,m);

r    = [down(:); 1; flipud(up(:))];
g    = [down(:); 1; flipud(up(:))];
b    = [down(:); 1; flipud(up(:))];
rgb  = [r g b];

