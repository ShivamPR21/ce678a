function hh = sphrwarp(a,option,radfac)

% SPHRWARP   Warps a matrix on a (distorted) unit sphere
%
% HOW h = sphrwarp(a,option,radfac)
%     h = sphrwarp(a,b,radfac)
%
% IN  a      - matrix to be visualized
%     option - 1 - spherical surface (default),
%              2 - spherical mesh,
%              3 - shaded deformed spherical surface
%              b - Matrix B is warped onto spherical surface, deformed by A. 
%     radfac - radial distortion factor [-1 1] (default 0)
% OUT h      - handle to surface object
%
% EG  . 
%       Ynm = ylm_rad(10,4); 
%       figure
%       sphrwarp(Ynm,3,.3);
%       figure %% gives Ilk-style spherical harmonic
%       sphrwarp(Ynm,3,.5);colormap([1 1 1; .5 .5 .5])     
%     . sphrwarp(ones(n,m),a) gives sphere with specified #meshlines
%     . h=sprhwarp(a,2,.5); shading interp, set(h,'EdgeColor','k')
%            may be useful for small matrices A.
%
% NB  The surface object appears small. Use MYZOOM or similar afterwards.
%
% Nico Sneeuw                        Munich                        August 1997

% uses SPHERE2
%
% Revision history
%   08/08/97/NS .based on the older version sphereplot
%   12/08/97/NS .a few changes in order to have proper colorbar settings
%   12/02/13/MA .example with the new SHBundle
% Notes
%  . Matrix A is used for surf (option 1), whereas the extended matrix Axt is
%    used for mesh (option 2). This allows 'shading interp' under option 2.

% diagnostics and defaults
if nargin < 4, dobar  = 0; end			% def: no colorbar
if nargin < 3, radfac = 0; end			% def: no deformation
if nargin < 2, option = 1; end			% def: surface plot
if length(option) > 1
   b = option;
   option = 4;
end
if ~any(option == (1:4)), error('Illegal OPTION'), end
if abs(radfac) > 1, error('RADFAC should be within [-1 1]'), end

% here we go
point = get(gcf,'Pointer');
set(gcf,'Pointer','watch')

% create spherical mesh
[m,n]   = size(a);
[x,y,z] = sphere2(m,n);

% Extend matrix A, and deform radius
if option == 2 | radfac ~= 0
   [az,el,r] = cart2sph(x,y,z);
   mid    = mean(mean(a));
   range  = max(max(a)) - min(min(a));
   axt    = a([1 1:m m],:);                     % matrix A extended
   axt    = axt(:,[1 1:n n]);
   axt    = conv2(axt,ones(2,2)/4,'valid');     % Axt now (m+1)x(n+1)
   radius = 1 + radfac*(axt - mid)/range;       % unit radius + deformation
   [x,y,z]= sph2cart(az,el,radius);
end

% do the graphics
if option == 1
   h = surf(x,y,z,a);
elseif option == 2
   h = mesh(x,y,z,axt);
elseif option == 3
   [az,el]=view;
   h = surfl(x,y,z,[az+135,-el]);
   if max(m,n) > 100
      shading flat
   else
      shading interp
   end
elseif option == 4
   newplot;					% because of low-level graphics
   h = surface(x,y,z,'FaceColor','texture','EdgeColor','k','CData',b);
   set(h,'UserData',[min(b(:)) max(b(:))])	% required for colorbar
end

% some post-processing
axis off
axis square
axis([-1 1 -1 1 -1 1])
set(gcf,'Pointer',point)

% do output argument
if nargout > 0, hh=h; end
