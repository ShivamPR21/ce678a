function [xx,yy,zz] = sphere2(m,n)
%SPHERE2 Generate sphere.
%	[X,Y,Z] = SPHERE2(M,N) generates three (m+1)-by-(n+1)
%	matrices so that SURF(X,Y,Z) produces a unit sphere.
%
%	[X,Y,Z] = SPHERE2 uses M = 18, N=36, a 10 degree equi-angular grid.
%
%	SPHERE2(N) and just SPHERE2 graph the sphere as a SURFACE
%	and do not return anything.
%
% To a large extent, this function is the same as the matlab sphere function.
% The main difference is that the size of the mesh, specified by sphere2
% is specified both in latitude and in longitude direction.
%
% Nico Sneeuw,
% November 1993

if nargin == 0, m = 18; n= 36; end
if nargin == 1, n= 2*m; end

% theta  (colatitude) is a column vector.
% lambda (longitude) is a row vector.
theta  = (0:m)'/m*pi;
lambda = (0:n)/n*2*pi;
ct = cos(theta);
st = sin(theta);  st(1) = 0; st(m+1) = 0;
cl = cos(lambda); 
sl = sin(lambda); sl(1) = 0; sl(n+1) = 0;

x      = st * cl;
y      = st * sl;
z      = ct * ones(1,n+1);

if nargout == 0
   surf(x,y,z)
else
   xx = x; yy = y; zz = z;
end
