% visualization tools, v.2013
%
%-----------------------------------------------------------------------
%			data visualization by color/symbol
%-----------------------------------------------------------------------
% barplotcol.m          - visualization of a 2D matrix in a "block-sytle"
%                         with additional options for the color  
% graypatch.m           - highlighing of periods with gray/white background
% plotcol.m             - visualization of 1D-3D signals, where the value 
%                         itself is displayed by the color
% plotcolm.m            - visualization on a map, where the value 
%                         itself is diplayed by the color
% plotcol4orbit.m       - adaption of plotcol for satellite data (3D visualization 
%                         and figure of the Earth)
% plotmulti.m           - visualization of several data sets with the same
%                         axes in one figure
% skyplot.m             - visualization of satellite observations or ephemerides in 
%                         the local horizontal frame
% sphrwarp.m            - projection of a 2D matrix onto a sphere 
% bubble_voroni.m       - calculates a kind of Voronoi diagram for given points 
%                         but cuts each Voronoi cell at a circular polygon around 
%                         each point
%
%-----------------------------------------------------------------------
%			colormaps
%-----------------------------------------------------------------------
% colbrew.m             - creates a colormap (color scheme depends on input parameters)
% gray1.m               - creates a colormap, ranging from black via withe to gray.
% gray2.m               - creates a colormap, ranging from gray via withe to black.
% redblue1.m            - creates a colormap, ranging from dark blue via white to dark red.
% redblue2.m            - creates a colormap, ranging from pastell blue via gray to pastell red.
% redblue3.m            - creates a colormap, ranging from dark blue via green and yellow to dark red.
% redblue4.m            - creates a colormap, ranging from dark blue via white to dark red.
% redblue5.m            - creates a colormap, ranging from dark blue via white to dark red.
% redblue6.m            - creates a colormap, ranging from pastell blue via green and yellow to pastell red. 
% redblue7.m            - creates a colormap, ranging from light blue via white to light red.
%
%-----------------------------------------------------------------------
%			auxilary tools
%-----------------------------------------------------------------------
% extendFrame.m         - increasing the axis of the figure (+/-10 percent of the data)
% centerColormap.m      - adaption of colorbar, so that positive and negative values are 
%                         visible separated
% sphere2.m             - enables different dimensions of rows and columns
%                         for the spherical representation of sphere.m
% symcaxis.m            - symmetric colorbar for equal order of magnitude,
%                         otherwise cut off at the maximum/minimum

