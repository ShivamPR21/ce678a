function rgb = redblue(typ, colmapsize, minmidmax)

% REDBLUE creates a colormap, different styles are possible. 
% 
% REDBLUE(typ)
% REDBLUE(typ, colmapsize)
% REDBLUE(typ, colmapsize, minmidmax)
%
% OPTIONAL:
%    typ .......... selects which colormap is produced:
%                   1: dark blue - white - dark red (default)
%                   2: mid blue - mid gray - mid red
%                   3: dark blue - cyan - white - yellow - dark red
%                   4: dark blue - light blue - white - light brown - dark red
%                   5: dark blue - light blue - white - orange - dark red
%                   6: pastel blue - greenish - yellowish - orange
%                   7: pastel blue - white - pastel red
%     colmapsize .. size of colormap (default: retrieved from set colormap)
%     maxmidmin ... maximal, middle and minimal value as array [max mid min]
%                   only needed for colorbars with a middle value not 
%                   centered on the scale. This feature will only work
%                   if max and min values are corresponding to your data. 
%                   (default: [1 0 -1])
% OUT:
%    rgb .......... colormap                                [colmapsize, 3]
%
% SEE ALSO:
%    COLBREW, GRAY

% -------------------------------------------------------------------------
% project: SHBundle 
% -------------------------------------------------------------------------
% authors:
%    Matthias ROTH (MR), GI, Uni Stuttgart 
%    Nico SNEEUW (NS), Munich
% -------------------------------------------------------------------------
% revision history:
%    2015-09-10: MR, combine all redblueX functions to a single redblue(X)
%    2002-12-10: NS, redblue6 based on colormap by Michael Schmidt,
%                    DGFI/OSU
%    ????-??-??: NS, redblue5
%    2001-02-21: NS, redblue4 by D. Kieke
%    1998-??-??: NS, redblue3 by Jody Klymak
%    ????-??-??: NS, redblue2 by Andrew Knight, Maritime Operations
%                    Division, Australia
%    1994-08-31: NS, redblue1, redblue7
% -------------------------------------------------------------------------
% license:
%    This program is free software; you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published by
%    the  Free  Software  Foundation; either version 3 of the License, or
%    (at your option) any later version.
%  
%    This  program is distributed in the hope that it will be useful, but 
%    WITHOUT   ANY   WARRANTY;  without  even  the  implied  warranty  of 
%    MERCHANTABILITY  or  FITNESS  FOR  A  PARTICULAR  PURPOSE.  See  the
%    GNU General Public License for more details.
%  
%    You  should  have  received a copy of the GNU General Public License
%    along with Octave; see the file COPYING.  
%    If not, see <http://www.gnu.org/licenses/>.
% -------------------------------------------------------------------------

%% define colormaps
% based on colormap by Nico Sneeuw
stations{1} = [ 1   0   0 150;    % dark blue
               10   0   0 255;    % blue
               32 255 255 255;    % white
               54 255   0   0;    % red
               64 139   0   0];   % dark red
             
% based on colormap by Andrew Knight             
stations{2} = [ 1  89 128 181;    % mid blue
               32 199 199 199;    % mid gray
               64 217  89  77];   % mid red

% based on colormap by Jody Klymak
stations{3} = [ 1   0   0 153;    % dark blue
               12   0   0 255;    % blue
               24   0 255 255;    % cyan
               32 255 255 255;    % white
               40 255 255   0;    % yellow
               52 255   0   0;    % red
               64 153   0   0];   % dark red

% based on colormap by D. Kieke
stations{4} = [ 1   0   0 128;    % dark blue
               16 128 192 255;    % light blue
               32 255 255 255;    % white
               48 255 192 128;    % light brown
               64 128   0   0];   % dark red

% based on colormap by Nico Sneeuw             
stations{5} = [ 1   0   0 128;    % dark blue
               16   0   0 255;    % blue
               20  24  89 255;    % mid blue
               24 114 178 255;    % light blue
               28 213 233 255;    % very light blue
               32 255 255 255;    % white
               36 255 242 231;    % very light orange
               40 255 195 141;    % light orange
               44 255 114  42;    % orange
               48 255   0   0;    % red
               64 128   0   0];   % dark red
             
% based on colormap by Michael Schmidt
stations{6} = [ 1 100  100  255;  % pastel blue
                2 107  107  252;
                3 117  117  252; 
                4 138  180  220;  % greenish
                5 160  220  190;  
                6 176  230  190; 
                7 208  240  190; 
                8 224  252  190;  % yellowish
                9 249  252  190; 
               10 255  255  180, 
               11 255  255  175; 
               12 255  255  100;  % light yellow
               13 255  191   50; 
               14 255  127   50; 
               15 255  100   30; 
               16 255   80   20]; % orange

% based on colormap by Nico Sneeuw         
stations{7} = [ 1 178 178 255;    % pastel blue
               32 255 255 255;    % white
               64 255 178 178];   % pastel red
             
%% check input parameters
if ~exist('typ', 'var')
    typ = 1;
end

if (typ < 1) || (typ > length(stations))
    warning(sprintf('"typ" must be a full number within [1, %d].\nNow switching to default values: typ = 1.', length(stations)));
    typ = 1;
end

if ~exist('colmapsize', 'var')
    colmapsize = size(get(gcf, 'colormap'), 1); 
end

if ~exist('maxmidmin', 'var')
    maxmidmin = [1 0 -1];
end

if max(size(maxmidmin) ~= [1 3])
    warning('"maxmidmin" must be a vector of the form [max mid min].\n Now switching to default values: [1 0 -1].');
    maxmidmin = [1 0 -1];
end
    
%% calculate colormap
m   = size(stations{typ}, 1);
x   = stations{typ}(:, 1);
m0  = mean(x);
mmm = (maxmidmin - maxmidmin(2)) / max(abs(maxmidmin([1 3]) - maxmidmin(2)));
mx  = mmm * (m0 - 1) + m0;

xi  = linspace(min(mx), max(mx), colmapsize);
rgb = interp1(x, stations{typ}(:, 2:4) / 255, xi, 'linear');

