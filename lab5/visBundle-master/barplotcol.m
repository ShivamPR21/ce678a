function HANDLE = barplotcol(H,ColorOption,maxRowCol)
% HANDLE = barplotcol(H,ColorOption,maxRowCol)
% HANDLE = barplotcol(H)
%
% BARPLOTCOL visualizes a data matrix as a "3D city block". The basic idea is
% the routine bar3.m, but the color is refering to the height information 
% instead of the column of the matrix. Zero elements, Inf and NaN are removed 
% from the graph
%
% IN:
% H:    2-dimensional data matrix                                       [-][NxN]
% ColorOption: choice between                                              ['string']
%       * 'floor':      color shows the different floors of the blocks
%       * 'building'    color shows the maximum value
% maxRowCol: option to increase the maximal number of columns and rows  [-][1x1]
%        default: no plot for more than 10000 elements
%
% OUT:
% HANDLE: handle of the graphics for further improvement
%
% See also: --
%
% sub-routines:
%    *   --
%
%
% REMARKS:
%    *   The size of the matrix is limited by the bar3.m function!
%    *   For more than 10000 elements, the bar-routine is very slow and you
%        get an warning. If you really want more, increase the limit by the
%        additional input of maxRowCol
%    *   example:
%        H = magic(10);
%        H = [H(1:5,:), -H(6:10,:)];
%        H(3,5) = NaN; H(3,7) = Inf;
%        H(5,1) = -Inf;H(1,9) = 0;
%        figure
%        subplot(231);barplotcol(H,'floor'); title('ColorOption: floor')
%        subplot(232);barplotcol(H','floor'); title('ColorOption: floor')
%        subplot(234);barplotcol(H,'building'); title('ColorOption: building')
%        subplot(235);barplotcol(H','building'); title('ColorOption: building')
%        subplot(2,3,[3,6]); bar3(H); title('bar3.m')


%**************************************************************************
% created at:       21.10.2010
% last modified:    12.02.2013 -> prush up help text 
%                   20.02.2013 -> renamed
% author:           Markus Antoni
% projekt:          preconditioning for radial basis functions
%**************************************************************************
if nargin<2
    ColorOption = 'building';
end
if nargin<3
    maxRowCol = 10000;
end

index = isnan(H);
H(index) = 0;
index = isinf(H);
H(index) = 0;

[row,column] = size(H);
if row*column>maxRowCol
    warning('amount of elements is quite large for bar3, increase >> maxRowCol << if you really want this')
    return
end


HANDLE = bar3(H,'detached');
row6 = row*6;
set(HANDLE,'FaceAlpha',.90);
set(gcf, 'Renderer', 'ZBuffer');
switch ColorOption
case 'building'
    
    for n=1:column

        index = logical(kron(H(:,n) == 0,ones(6,1)));
        colorData = get(HANDLE(n),'ZData');
        colorData(index,:) = nan;
        
        for k = 1:6:row6;
            colordata1 = colorData(k:k+5,:);
            index1 = colordata1 == 0;
            colordata1(index1) = colordata1(2,2);

            colorData(k:k+5,:) = colordata1;
        end
        set(HANDLE(n),'cData',colorData);
    
    end    
case 'floor'
    for n=1:column

        index = logical(kron(H(:,n) == 0,ones(6,1)));
        colorData = get(HANDLE(n),'ZData');
        colorData(index,:) = nan;
        set(HANDLE(n),'cData',colorData);

    
    
    end
   
otherwise
    warning('ColorOption is not implemented')
end


shading interp; axis tight; 
colorbar

